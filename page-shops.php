<?php 

/*
	Template Name: Dystrybutorzy
*/

?>


<?php get_header(); ?>

<?php get_template_part('templates/template-breadcrumbs'); ?>

<?php 

$args = array(
	'orderby'          => 'date',
	'order'            => 'DESC',
	'post_type'        => 'dystrybutorzy',
	'post_status'      => 'publish',
	'suppress_filters' => true,
	'posts_per_page' => $post_limit
);

$shops = new WP_Query($args);

?>

<style>
    .page-phone {
        display: none;
    }
</style>

<main role="main" class="page-content">

	<section class="wrapper">
		<div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-title">
                        <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( '%s', 'twentyten' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
                    </h1>

                    <!-- <p>Przedstawiamy mapę dystrybutorów produktów Guide Sensmart. Kliknij na mapie w dystrybutora, aby wyświetlić dane teleadresowe.</p> -->
                </div>
            </div>

            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <div class="row">
                    <div class="col-12">
                        <article id="post-<?php the_ID(); ?>">
                            <div class="page-wysiwig">
                                <div class="page-wysywig-spacer"></div>
                                <?php the_content(); ?>
                            </div>
                        
                            <?php edit_post_link('Edytuj stronę'); ?>
                        </article>
                    </div>
                </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>

        <script>
            window.activeMarker = undefined;
            
            function initMapShop() {

                var styles = [{"elementType": "geometry", "stylers": [{"color": "#f5f5f5"} ] }, {"elementType": "labels.icon", "stylers": [{"visibility": "off"} ] }, {"elementType": "labels.text.fill", "stylers": [{"color": "#616161"} ] }, {"elementType": "labels.text.stroke", "stylers": [{"color": "#f5f5f5"} ] }, {"featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{"color": "#bdbdbd"} ] }, {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#eeeeee"} ] }, {"featureType": "poi", "elementType": "labels.text.fill", "stylers": [{"color": "#757575"} ] }, {"featureType": "poi.park", "elementType": "geometry", "stylers": [{"color": "#e5e5e5"} ] }, {"featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"} ] }, {"featureType": "road", "elementType": "geometry", "stylers": [{"color": "#ffffff"} ] }, {"featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{"color": "#757575"} ] }, {"featureType": "road.highway", "elementType": "geometry", "stylers": [{"color": "#dadada"} ] }, {"featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [{"color": "#616161"} ] }, {"featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"} ] }, {"featureType": "transit.line", "elementType": "geometry", "stylers": [{"color": "#e5e5e5"} ] }, {"featureType": "transit.station", "elementType": "geometry", "stylers": [{"color": "#eeeeee"} ] }, {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#c9c9c9"} ] }, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"} ] } ];

                var center = {
                    lat: 51.614628, lng: 21.120829
                }

                // Create a map object and specify the DOM element for display.
                this.mapShop = new google.maps.Map(document.getElementById('mapShop'), {
                    center: center,
                    scrollwheel: false,
                    mapTypeControl: true, // false
                    zoom: 6,
                    disableDefaultUI: false // true
                });

                if (window.innerWidth < 768) {
                    this.mapShop.setCenter({
                        lat: 51.414628, 
                        lng: 21.120829
                    })
                }
                
                var locations = [
                    <?php 
                        if ($shops->have_posts() ) {
                            while ( $shops->have_posts() ) : $shops->the_post();
                                $id = get_the_ID();
                                $lat = get_post_meta($id, 'shops_lat', true);
                                $lng = get_post_meta($id, 'shops_lng', true);

                                if ($lat && $lng) {
                            ?>
                                {
                                    id: <?php echo $id; ?>,
                                    position: {
                                        lat: <?php echo $lat; ?>, 
                                        lng: <?php echo $lng; ?>
                                    }
                                },
                            <?php }
                            endwhile; 
                        };

                        wp_reset_postdata();

                    ?>
                ];
                
                var markers = locations.map(function(location, i) {
                    return new google.maps.Marker({
                        position: location.position,
                        id: location.id
                    });
                });

                var markerCluster = new MarkerClusterer(this.mapShop, markers, {
                    imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
                });

                markers.forEach(function(element) {
                    // var infowindow = new google.maps.InfoWindow({
                    //     content: 'Test'
                    // });

                    google.maps.event.addListener(element, 'click', function() { 
                        // infowindow.open(this.mapShop, element);
                        let id = this.id;
                        window.showShop(id);

                        if (window.activeMarker && window.activeMarker.id == element.id) {
                            console.log('same')
                        } else {
                            if (window.activeMarker && window.activeMarker.getAnimation() !== null) {
                                window.activeMarker.setAnimation(null);
                            }
                            
                            window.activeMarker = element;
                            window.activeMarker.setAnimation(google.maps.Animation.BOUNCE);

                            this.mapShop.setCenter({
                                lat: window.activeMarker.lat, 
                                lng: window.activeMarker.lng
                            })
                        }
                    });
                });

                // var marker = new google.maps.Marker({
                //     map: this.mapShop,
                //     icon: '<?php echo get_template_directory_uri(); ?>'+'/assets/img/google-maps-marker-small.png',
                //     position: {lat: 51.414628, lng: 21.120829}
                // });

                // google.maps.event.addListener(marker_5, 'mouseout', function() { 
                //         infowindow_5.close(this.map, marker_5);
                // });
            }
            
        </script>
        
        <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxIauAclNMt92h1U0FdmlJFN30rMAdYIg&callback=initMapShop" async defer></script>

        <div class="page-shops__mapOuter">
            <div class="page-shops__entries">
                
            </div>

            <div class="page-shops__map">
                <div class="page-shops__mapMore">
                    <a href="#" class="page-shops__mapMoreClose">+</a>

                    <div class="page-shops__mapMoreContent"></div>
                </div>
                <div style="margin-bottom: 30px; height: 600px;" id="mapShop"></div>
            </div>
        </div>

        <div class="container hidden">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-wysiwig">
                        
                        <?php get_template_part('templates/template-shops-all'); ?>
                    </div>
                </div>
            </div>
        </div>
	</section>
</main>

<?php get_footer(); ?>