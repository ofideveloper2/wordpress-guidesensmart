1. Repo zawiera tylko motyw strony

Strona WWW http://guideir.com.pl/ zawiera instalację dwóch Wordpressów:
 a) http://guideir.com.pl/
 b) https://www.guideir.com.pl/kamery-termowizyjne/

Aktualizacje plików należy dokonywać w obu folderach na FTP.

Natomiast lokalnie pracuję na jednej bazie i jednym motywie.
Przełączanie między wersjami jest dostępne w Ustawieniach motywu w CMS.

2. Ustaw serwer Wordpress, zgodnie adresem np. http://sensmart.test:8888/ (gulpfile.js)
- gulp build => tworzenie zminifikowanych plików

3. Z FTP należy przenieść Wordpress wraz z zawartością folderu /uploads

4. Dostęp do bazy / bazę wyślę osobno

5. Commity prosiłbym pisać po angielsku w czasie przeszłym

6. Pliki api-guarantee.php oraz form_request.php są w repo, natomiast na FTP 
są w katalogu głównym Wordpress (łączę się z Wordpress, stąd ta sytuacja)

==========================

W razie pytań: dominik@urlstudio.pl
