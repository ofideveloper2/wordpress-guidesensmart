<?php 

require_once(dirname(__FILE__) . '/wp-config.php');
$wp->init();
$wp->parse_request();
$wp->query_posts();
$wp->register_globals();
$wp->send_headers();

$to = "dominik@urlstudio.pl";
$isChecked = "NIE";
$isCheckedRules = "NIE";

$reply = "dominik@urlstudio.pl";
$name = "";
$from_name = "guidesensmartpl@gmail.com";

// "jakub.rubis@knieja.com.pl"

if (isset($_REQUEST['giodo']) && $_REQUEST['giodo'] == 'on') { 
	$isChecked = "TAK";
}

if ($_REQUEST['email'] && $_REQUEST['email'] !== '') { 
	$reply = $_REQUEST['email'];
}

if ($_REQUEST['name'] && $_REQUEST['name'] !== '') {
	$name = $_REQUEST['name'];
}

$subject = "** FORMULARZ KONTAKTOWY ** {$name}";

function utf8mail($to, $s, $body, $from_name, $reply) {
	$s= "=?utf-8?b?".base64_encode($s)."?=";
	$headers = "From: =?utf-8?b?".base64_encode($from_name)."?= <".$reply.">\r\n";
	$headers.= "Content-Type: text/plain;charset=utf-8\r\n";
	$headers.= "Reply-To: $reply\r\n";
	$headers.= "X-Mailer: PHP/" . phpversion();
	return wp_mail($to, $s, $body, $headers);
}

$body = "Imię: {$_REQUEST['name']} \r\n".
		"Kontakt: {$_REQUEST['phone']}\r\n".
		"Wiadomość:\r\n {$_REQUEST['message']}\r\n\r\n".
		"Wyrażam zgodę na przetwarzanie danych osobowych w celu kontaktu: {$isChecked}";

$fields = array('name'=>'Imię', 'phone'=>'Telefon', 'giodo'=>'Obowiązek informacyjny');

foreach($fields as $key => $val){
	if(empty($_REQUEST[$key])){
		die('{"status":false}');
	}
}

// MESSAGE TO AUTHOR
$send = utf8mail($to, $subject, $body, $from_name, $reply);
// $send3 = utf8mail('hello@urlstudio.pl', $subject, $body, $from_name, $reply);

if ($send) {
	print_r('{"status":true}');
} else {
	print_r('{"status":false}');
}

?>
