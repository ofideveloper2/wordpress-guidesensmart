<?php 

/*
	Template Name: Gwarancja
*/

?>

<?php get_header(); ?>

<?php get_template_part('templates/template-breadcrumbs'); ?>

<main role="main" class="page-content">
	<section class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="page-title">
				  <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( '%s', 'twentyten' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
				</h1>
			</div>
        </div>

        <?php if (sensmart_get_option('skus')) { ?>
        <div class="row">
            <div class="col-md-12">
                <h2 class="mb-0">
                    Sprawdź czy Twoje urządzenie pochodzi z&nbsp;oficjalnej dystrybucji
                </h2>
                <p class="">
                    Wpisz numer seryjny urządzenia
                    i sprawdź czy jest ono objęte wygodnym systemem napraw w sieci F. H. Knieja:
                </p>

                <div>
                    <input type="text" data-action="<?php echo get_home_url(); ?>/check_api-guarantee.php" class="input-sku" placeholder="Wpisz numer seryjny">
                </div>

                <div>
                    <a href="#" class="button button-small btn-skuSearch">
                        <span class="button-text">
                            Sprawdź
                        </span>
                        <span class="button-bg"></span>
                    </a>
                </div>

                <div class="skus-messages hidden">
                    <p class="skus-messages__success hidden">
                        Tak! Twoje urządzenie jest objęte gwarancją.
                        W razie problemów z&nbsp;urządzeniem zapraszamy do <a href="https://www.knieja.com.pl/kontakt,12" class="link-underline" target="_blank">
                            kontaktu &rarr;
                        </a>.
                    </p>
                    <p class="skus-messages__error hidden">
                        Niestety, prawdopodobnie Twoje urządzenie pochodzi spoza oficjalnej dystybucji.
                        Prosimy o <a href="https://www.knieja.com.pl/kontakt,12" target="_blank" class="link-underline">kontakt &rarr;</a>.
                    </p>

                    <p class="skus-messages__api_error hidden">
                        Wystąpił błąd w&nbsp;połaczeniu. Prosimy odświeżyć stronę i spróbować ponownie później.
                    </p>
                    
                    <p class="skus-messages__tooShort hidden">Prosimy wpisać pełny numer seryjny.</p>
                </div>
            </div>
        </div>
        <?php } ?>
        
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<div class="row">
				<div class="col-12">
					<article id="post-<?php the_ID(); ?>">
						<div class="page-wysiwig">
							<div class="page-wysywig-spacer"></div>
							<?php the_content(); ?>
						</div>
					
						<?php edit_post_link('Edytuj stronę'); ?>
					</article>
				</div>
			</div>
		<?php endwhile; ?>
        <?php endif; ?>
	</section>
</main>

<?php get_footer(); ?>