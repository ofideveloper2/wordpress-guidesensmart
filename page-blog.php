<?php 

/*
	Template Name: Blog
*/

?>

<?php get_header(); ?>


<main role="main" class="page-content">
	<section class="container">
        <h1 class="d-blog-title"> <?= the_title(); ?> </h1> 


<?php
$d_args = array(
    'post_type' => 'd-post',
    'posts_per_page' => -1,

);

$d_posts = new WP_Query($d_args);
?>

<?php if($d_posts->have_posts()) : ?>
    <?php while($d_posts->have_posts()) : $d_posts->the_post(); ?>
    <?php  if($i % 3 == 0)  : ?>

<div class="row">
<?php endif; ?> 


    <div class="col-md-4 col-sm-12" style="margin-bottom: 20px;">
        <div class="post">
            <div class="thumb">
                <a rel="nofollow" href="<?= the_permalink(); ?>" > <img src="<?= the_post_thumbnail_url() ?>" width='750' height='420' /> </a>
            </div>
        
            <div class="title">
                <a href="<?php the_permalink(); ?>"> <h2 class="d-post-title" >  <?php the_title(); ?> </h2> </a>
            </div>
            <div class="meta">
                <span> <?= get_the_author_meta('first_name');?>  | </span>
                <span> <?= the_date('Y-m-d') ?> </span> 
            </div>
            <div class="excerpt">
                <?= the_excerpt(); ?>
            </div>
          
        </div>
    </div>

                <?php $i++; 
if($i != 0 && $i % 3 == 0) { ?>
    </div><!--/.row-->
    <div class="clearfix"></div>

<?php
} ?>

 <?php   
    endwhile;
    endif;
    // Reset postdata
    wp_reset_postdata();
?>
        




    </sectiom> 
</main>











<?php get_footer(); ?>