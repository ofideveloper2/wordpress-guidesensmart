<?php get_header(); ?>

<?php get_template_part('templates/template-breadcrumbs'); ?>

<main role="main" class="page-content">
	<section class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="page-title">
				  <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( '%s', 'twentyten' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
				</h1>
			</div>
		</div>
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<div class="row">
				<div class="col-12">
					<article id="post-<?php the_ID(); ?>">
						<div class="page-wysiwig">
							<div class="page-wysywig-spacer"></div>
							<?php the_content(); ?>
						</div>
					
						<?php edit_post_link('Edytuj stronę'); ?>
					</article>
				</div>
			</div>
		<?php endwhile; ?>
		<?php endif; ?>
	</section>
</main>

<?php get_footer(); ?>