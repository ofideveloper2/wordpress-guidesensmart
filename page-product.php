<?php 

/*
	Template Name: Products
*/

?>

<?php get_header(); ?>

<?php get_template_part('templates/template-breadcrumbs'); ?>

<main role="main" class="page-content">
	<section class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="page-title">
					<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( '%s', 'twentyten' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
				</h1>
			</div>
		</div>
		
		<?php get_template_part('templates/template-products-all'); ?>

	</section>
</main>

<?php get_footer(); ?>