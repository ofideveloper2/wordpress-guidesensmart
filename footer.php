
	<div class="page-facebook">
		<a href="https://www.facebook.com/guidetermowizja/" target="_blank" title="Facebook Guide Termowizja">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook.svg" alt="Facebook Knieja Termowizja">
		</a>
	</div>

	<div class="page-phone">

		<?php if (sensmart_get_option('is_camera')) { ?>
		<a href="tel:+48691449964" title="Zadzwoń">
			<span class="page-phone-icon">
				<svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1600 1240q0 27-10 70.5t-21 68.5q-21 50-122 106-94 51-186 51-27 0-52.5-3.5t-57.5-12.5-47.5-14.5-55.5-20.5-49-18q-98-35-175-83-128-79-264.5-215.5t-215.5-264.5q-48-77-83-175-3-9-18-49t-20.5-55.5-14.5-47.5-12.5-57.5-3.5-52.5q0-92 51-186 56-101 106-122 25-11 68.5-21t70.5-10q14 0 21 3 18 6 53 76 11 19 30 54t35 63.5 31 53.5q3 4 17.5 25t21.5 35.5 7 28.5q0 20-28.5 50t-62 55-62 53-28.5 46q0 9 5 22.5t8.5 20.5 14 24 11.5 19q76 137 174 235t235 174q2 1 19 11.5t24 14 20.5 8.5 22.5 5q18 0 46-28.5t53-62 55-62 50-28.5q14 0 28.5 7t35.5 21.5 25 17.5q25 15 53.5 31t63.5 35 54 30q70 35 76 53 3 7 3 21z"/></svg>
			</span>
			<span class="page-phone-text">
				<strong>Masz pytania? Zadzwoń:</strong>
				+48 691 449 964
			</span>
		</a>
		<?php } else { ?>
		<a href="tel:+48124218233" title="Zadzwoń">
			<span class="page-phone-icon">
				<svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1600 1240q0 27-10 70.5t-21 68.5q-21 50-122 106-94 51-186 51-27 0-52.5-3.5t-57.5-12.5-47.5-14.5-55.5-20.5-49-18q-98-35-175-83-128-79-264.5-215.5t-215.5-264.5q-48-77-83-175-3-9-18-49t-20.5-55.5-14.5-47.5-12.5-57.5-3.5-52.5q0-92 51-186 56-101 106-122 25-11 68.5-21t70.5-10q14 0 21 3 18 6 53 76 11 19 30 54t35 63.5 31 53.5q3 4 17.5 25t21.5 35.5 7 28.5q0 20-28.5 50t-62 55-62 53-28.5 46q0 9 5 22.5t8.5 20.5 14 24 11.5 19q76 137 174 235t235 174q2 1 19 11.5t24 14 20.5 8.5 22.5 5q18 0 46-28.5t53-62 55-62 50-28.5q14 0 28.5 7t35.5 21.5 25 17.5q25 15 53.5 31t63.5 35 54 30q70 35 76 53 3 7 3 21z"/></svg>
			</span>
			<span class="page-phone-text">
				<strong>Masz pytania? Zadzwoń:</strong>
				+48 12 421 82 33
			</span>
		</a>

		<?php } ?>
	</div>

	<?php if (!sensmart_get_option('is_camera') && is_front_page()) { ?> 
		<img class="page-decorHunting" src="<?php echo get_template_directory_uri(); ?>/assets/img/hunting-large-bg.jpg" alt="">
	<?php } ?>
		
	<footer class="footer">

		<div class="container">
			<div class="row">
				
			</div>

			<div class="row">
				<div class="col-md-6">

					<h2 class="footer-logo">
						<a href="<?php echo get_site_url(); ?>" title="<?php echo get_bloginfo('name'); ?> | Sklep online">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-white.png" alt="<?php echo get_bloginfo('name'); ?> | Sklep online">
						</a>
					</h2>

					<h2 class="footer-rightTitle">
						Dystrybutor na terenie Polski:

						<a href="https://knieja.com.pl" title="Dystrybutor Urządzeń Termowizyjnych | F.H. Knieja">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/knieja-white.png" alt="Dystrybutor Urządzeń Termowizyjnych | F.H. Knieja">
						</a>
						
					</h2>

					<p>
						<span>Firma Handlowa "Knieja"</span>
						<span>ul. Daszyńskiego 32</span>
						<span>31-534 Kraków</span>
					</p>

					<h3></h3>
					
					<p class="footer-contact">
						<span>
							<?php if (sensmart_get_option('is_camera')) { ?>
							<a href="tel:+48691449964" title="Telefon - Dystrybutor Urządzeń Termowizyjnych | F.H. Knieja">Zadzwoń: +48 691 449 964</a>
							<?php } else { ?>
							<a href="tel:+48124218233" title="Telefon - Dystrybutor Urządzeń Termowizyjnych | F.H. Knieja">Zadzwoń: +48 12 421 82 33</a>
							<?php } ?>
						</span>
					</p>
				</div>

				<?php
					$terms = get_terms( array(
						'taxonomy' => 'type',
						'hide_empty' => false,
					) );
				?>
				
				<div class="col-md-6">
					<?php if ($terms) { ?>
					<h3 class="mt-">
						Urządzenia termowizyjne:
					</h3>

					
					<div class="footer-menu">
						<ul>
							<?php foreach ($terms as &$term) { ?>
								<li>
									<a href="<?php echo get_term_link($term); ?>" title="Termowizja: <?php echo $term->name; ?> | Sklep online">
										<?php echo $term->name; ?>
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
					<?php } ?>

					<div class="footer-menu footer-menu-meta">
						<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
					</div>
					
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<p class="footer-copyright">
							Copyright &copy; 2011 - <?php echo date("Y"); ?> <br>
							WWW.GUIDEIR.COM.PL All Rights Reserved
							Wuhan Guide Sensmart Tech Co., Ltd.
							<br>
							<a href="<?php echo get_site_url(); ?>" title="F.H. Knieja Sklep online Termowizja">
								F.H. Knieja |  Sklep online Termowizja
							</a>
						</p>
				</div>
			</div>
		</div>
	</footer>

	<script src="<?php echo get_template_directory_uri(); ?>/js/bundle.js"></script>

	<?php wp_footer(); ?>
</body>
</html>