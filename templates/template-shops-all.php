<?php 

$post_limit = 9999;

if ($template_args['limit']) {
	$post_limit = $template_args['limit'];
}

// if (is_front_page()) {
// 	$post_limit = 3;
// }

$args = array(
	'orderby'          => 'date',
	'order'            => 'DESC',
	'post_type'        => 'dystrybutorzy',
	'post_status'      => 'publish',
	'suppress_filters' => true,
	'posts_per_page' => $post_limit
);

if ($template_args['query_arr']) {
	$args = $template_args['query_arr'];
}

$the_query = new WP_Query($args);

?>


<div class="row">
    <?php 
        if ($the_query->have_posts() ) {
            while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <?php
                    $postID = get_the_ID();
                    
                    $title = get_post_meta($postID, 'shops_title', true);
                    $title_converted = str_replace('"', '', $title);
                    
                    $shops_street = get_post_meta($postID, 'shops_street', true);
                    $shops_zipcode = get_post_meta($postID, 'shops_zipcode', true);
                    $shops_city = get_post_meta($postID, 'shops_city', true);

                    $shops_www = get_post_meta($postID, 'shops_www', true);
                    $shops_phone = get_post_meta($postID, 'shops_phone', true);
                    $shops_phone2 = get_post_meta($postID, 'shops_phone2', true);
                    $shops_email = get_post_meta($postID, 'shops_email', true);

                    $shops_region = wp_get_post_terms($postID, 'wojewodztwo');
                    $shops_region = $shops_region[0];
                    
                ?>

                <div class="<?php if ($template_args['limit']) { echo 'col-md-6 col-lg-4'; } else { echo 'col-lg-6'; } ?>">
                    <!-- <?php the_permalink(); ?> -->
                    <div data-shopid="<?php echo get_the_ID(); ?>" 
                        title="Sklep z termowizją - <?php echo $title; ?>, miasto: <?php echo $shops_city; ?>" class="page-shops__preview">

                        <h4 class="page-shops__preview-title mt-0 mb-0">
                            <span><?php echo $title; ?></span>
                        </h4>

                        <p class="page-shops__preview-address">
                            <span class="d-block"><?php echo $shops_street; ?></span>
                            <span class="d-block"><?php echo $shops_zipcode; ?> <span><?php echo $shops_city; ?></span></span>
                        </p>

                        <p class="link-red">
                            <?php if ($shops_www) { ?>
                            <span class="d-block">WWW: <a target="_blank" href="<?php echo $shops_www; ?>/?utm_source=guideir.com.pl"><?php echo $shops_www; ?></a></span>
                            <?php } ?>
                            <?php if ($shops_email) { ?>
                            <span class="d-block">E-mail: <a href="mailto:<?php echo $shops_email; ?>?subject=Zapytanie o termowizję Guide Sensmart"><?php echo $shops_email; ?></a></span>
                            <?php } ?>
                            <?php if ($shops_phone) { ?>
                            <span class="d-block">
                                Telefon:
                                <a href="tel:<?php echo $shops_phone; ?>"><?php echo $shops_phone; ?></a>
                            </span>
                            <?php } ?>

                            <?php if ($shops_phone2) { ?>
                                <span class="d-block">
                                    Telefon:
                                    <a href="tel:<?php echo $shops_phone2; ?>"><?php echo $shops_phone2; ?></a>
                                </span>
                            <?php } ?>
                        </p>

                        <p>
                            <a href="https://www.google.pl/maps/search/<?php echo $title_converted; ?>+<?php echo $shops_city; ?>//data=" class="button button-small" target="_blank">
                                <span class="button-text">Wskazówki dojazdu &rarr;</span>
                                <span class="button-bg"></span>
                            </a>
                        </p>

                        <!-- <p class="portfolio-preview-desc">
                            <?php if ($shops_region) { ?>
                                <span>Województwo: <?php echo $shops_region->name; ?> </span>
                            <?php } ?>
                        </p> -->
                    </div>
                </div>

            <?php endwhile;
        } else {
            echo '<div class="col-md-12"><p>Realizacje w trakcie dodawania.</p></div>';
        };
        
        wp_reset_postdata();
    ?>
</div>