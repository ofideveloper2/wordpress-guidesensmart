<div class="page-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php if (!is_front_page()) { ?>
                    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
                        <?php if(function_exists('bcn_display')) {
                            bcn_display();
                        } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>