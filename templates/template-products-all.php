<?php 

$post_limit = 100;

if ($template_args['limit']) {
	$post_limit = $template_args['limit'];
}
// if (is_front_page()) {
// 	$post_limit = 3;
// }

$args = array(
	'orderby'          => 'date',
	'order'            => 'DESC',
	'post_type'        => 'product',
	'post_status'      => 'publish',
	'suppress_filters' => true,
	'posts_per_page' => $post_limit
);

if ($template_args['query_arr']) {
	$args = $template_args['query_arr'];
}

$the_query = new WP_Query($args);

?>

<div class="row">
	<?php 
		if ($the_query->have_posts() ) {
			while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php
					$postID = get_the_ID();
					$image = get_post_meta($postID, 'product_image', true);
					$product_price = get_post_meta($postID, 'product_price', true);
					
					// $product_type = wp_get_post_terms($postID, 'type');
					// $product_type = $product_type[0];

					// $product_serie = wp_get_post_terms($postID, 'series');
					// $product_series = $product_serie[0];
				?>

				<div class="col-md-6 col-lg-3">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="product-preview">
						<div class="product-preview-image d-flex align-items-center">
							<?php if ($image) { ?>
								<img src="<?php echo $image; ?>" alt="<?php the_title(); ?>">
							<?php } else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-no-image.png" alt="<?php echo $term->name; ?> - producent termowizja sklep online">
							<?php } ?>
							<i class="iconfont f66 col1"></i>
						</div>
						<h4 class="product-preview-title">
							<?php the_title(); ?>
						</h4>

						<?php if ($product_price) { ?>
						<p class="product-price">
							<span><?php echo $product_price; ?></span>
						</p>
						<?php } ?>
					</a>
				</div>

			<?php endwhile;
		} else {
			echo '<div class="col-md-12"><p>Brak produktów.</p></div>';
		};
		
		wp_reset_postdata();
	?>
</div>