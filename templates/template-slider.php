<?php
	$postID = get_the_ID();
	$slider = get_post_meta($postID, 'homepage_slider', true);
	$count = 0;
?>

<?php if ($slider) { ?>
<div class="slider">
	<?php foreach ($slider as &$slide) { ?>
		<!-- <div class="slider-box" style="background-image: url(<?php echo $slide['image']; ?>)"> -->
		<div class="slider-box">
			<div class="slider-slide">
				<?php if ($slide['button_url']) { ?>
					<a  href="<?php echo $slide['button_url']; ?>">
				<?php } ?>
					<picture>
						<source media="(max-width: 768px)" srcset="<?php echo $slide['image_mobile']; ?>">
						<source media="(min-width: 768px)" srcset="<?php echo $slide['image']; ?>">
						<img src="<?php echo $slide['image']; ?>" alt="<?php echo $slide['title']; ?>">
					</picture>
				<?php if ($slide['button_url']) { ?>
					</a>
				<?php } ?>
				
				<!-- <div class="slider-content <?php if ($count == 0) { echo 'animate'; } ?>">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<h3 class="slider-title"><?php echo $slide['title']; ?></h3>
								
								<?php if ($slide['desc']) { ?> 
									<p class="slider-text text-lead"><?php echo $slide['desc']; ?></p>
								<?php } ?>
								
								<?php if ($slide['button_url']) { ?>
									<a class="button slider-cta" href="<?php echo $slide['button_url']; ?>" title="<?php echo $slide['button_copy']; ?>">
										<span class="button-text">
											<?php echo $slide['button_copy']; ?>
										</span>
										<span class="button-bg"></span>
									</a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div> -->
			</div>
		</div>

		<?php $count++; ?>
	<?php } ?>
</div>
<?php } ?>