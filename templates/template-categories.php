<?php 

$_terms = get_terms( 'type', array('hide_empty' => true, 'parent' => 0) );

?>

<div class="row d-flex justify-content-sm-center">
    <?php if ($_terms) { ?>
        <?php foreach ($_terms as $term) { 
            $term_id =  $term->term_id;
            $image = get_term_meta($term_id, 'type_image', true); ?>

                <div class="col-md-6 col-lg-3">
					<a href="<?php echo get_term_link($term_id); ?>" title="<?php echo $term->name; ?> - producent" class="product-preview">
						<div class="product-preview-image d-flex align-items-center">
							<?php if ($image) { ?>
								<img src="<?php echo $image; ?>" alt="<?php echo $term->name; ?> - producent termowizja sklep online">
							<?php } else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-no-image.png" alt="<?php echo $term->name; ?> - producent termowizja sklep">
							<?php } ?>
							<i class="iconfont f66 col1"></i>
						</div>
						<h4 class="product-preview-title">
                            <?php echo $term->name; ?>
						</h4>
					</a>
				</div>
        <?php } ?>
    <?php } ?>
</div>