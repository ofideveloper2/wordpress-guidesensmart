<?php

/*
	Template Name: Homepage
*/

?>

<?php get_header(); ?>

<?php get_template_part('templates/template-slider'); ?>

<main role="main" class="page-content">

	<?php if (sensmart_get_option('is_camera')) { ?>

	<section id="produkty">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title-borderOuter">
						<h1 class="title-border">
							<a href="<?php the_permalink(); ?>" title="Kamera termowizyjna do masowego pomiary temperatury ciała"><strong>Kamera termowizyjna do masowego <br>pomiaru temperatury ciała</strong> <br>Guide IR236</a>
						</h1>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="benefit-table">
						<div class="benefit-tableCell page-wysiwig">
							<h3>Zalety kamery</h3>
							<ul>
								<li>System przetestowany i&nbsp;sprawdzony na ponad <strong>10 milionach osobnych pomiarów</strong></li>
								<li>Jest to&nbsp;idealne urządzenie do&nbsp;szybkiego i&nbsp;efektywnego redukowania ryzyka transmisji wirusowej w miejscach zwiększonego ruchu pieszych takich jak: zakłady pracy, galeria handlowe, lotniska, stacje kolejowe i&nbsp;inne miejsca użytku publicznego</li>
								<li>Gwarantuje szybkość i&nbsp;co najważniejsze, <strong>niezawodność pomiarów</strong> (przepustowość 500 osób na minutę)</li>
								<li>Dyskretny pomiar z&nbsp;zasięgem aż do 8 metrów</li>
								
								<li>Stosowany m.in. na jednym z&nbsp;największych lotnisk na świecie - <strong>Lotnisko Beijing Daxing Int’l Airport</strong>, i wielu innych miejscach</li>
							</ul>
							<p>
								<a class="button button-small scrollTo" href="https://www.guideir.com.pl/kamery-termowizyjne/product/kamera-termowizyjna-do-masowego-pomiaru-temperatury-ciala/" title="Kamera termowizyjna do masowego pomiaru temperatury ciała, sklepy wielkopowierzchniowe">
									<span class="button-text">
										Czytaj więcej
									</span>
									<span class="button-bg"></span>
								</a>
							</p>
						</div>
					</div>
				</div>

				<div class="col-lg-5 col-md-6 offset-lg-1">
					<div class="benefit-table">
						<div class="benefit-tableCell page-wysiwig">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/guide-ir236.jpg" alt="Img - 0">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php } else { ?>
		<section id="produkty">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="title-borderOuter">
							<h2 class="title-border">Co zabierzesz na&nbsp;polowanie?</h2>
						</div>
					</div>
				</div>

				<?php hm_get_template_part('templates/template-categories'); ?>
			</div>
		</section>
	<?php } ?>

	<section class="section-company">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="section-company__content">
						<h3>
							Guide Sensmart - pionier w&nbsp;produkcji wysokiej jakości
							kamer termowizyjnych oraz systemów optoelektronicznych
							z&nbsp;własnym działem R&D dla firm z&nbsp;branż: energetycznej,
							petrochemicznej, budowlanej, ochrony PPOŻ,
							zabezpieczeń i&nbsp;innych.
						</h3>
						<p>
							<a class="button button-small scrollTo" href="#produkty" title="Termowizja sklep online - produkty">
								<span class="button-text">
									Sprawdź pełną ofertę
								</span>
								<span class="button-bg"></span>
							</a>
						</p>
					</div>

				</div>
			</div>
		</div>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/company-large.png" alt="Kamery termowizyjne | Guide Sensmart">
	</section>

	<div class="container">
		<section>
			<div class="row">
				<div class="col-md-12">
					<div class="title-borderOuter">
						<h2 class="title-border">Guide Sensmart</h2>
					</div>
				</div>
			</div>

			<div class="row section-about">
				<div class="col-sm-4 col-xs-6">
					<div class="section-about__block text-center">
						<div class="section-about__icon col1">
							<i class="iconfont"></i>
						</div>
						<h3 class="section-about__title f16">
							Ponad 20 lat doświadczenia
						</h3>
						<p class="section-about__info f12">
							Wuhan Guide Infrared Co., właściciel marki Guide Sensmart,
							powstało&nbsp;w&nbsp;roku 1999.
						</p>
					</div>
				</div>
				<div class="col-sm-4 col-xs-6">
					<div class="section-about__block text-center">
						<div class="section-about__icon col1">
							<i class="iconfont"></i>
						</div>
						<h3 class="section-about__title f16">
							Ciągły rozwój, zaawansowana technologia
						</h3>
						<p class="section-about__info f12">
							Niezależny dział badań i&nbsp;rozwoju złożony
							z&nbsp;ponad 1000 ekspertów
							pozwala na produkcję wysokiej
							jakości&nbsp;rozwiązań, które z generacje na generację oferują
							coraz nowocześniejsze technologie.
						</p>
					</div>
				</div>
				<div class="col-sm-4 col-xs-6">
					<div class="section-about__block text-center">
						<div class="section-about__icon col1">
							<i class="iconfont"></i>
						</div>
						<h3 class="section-about__title f16">
							Rozbudowana sieć sprzedaży, <br>
							ponad 200 patentów
						</h3>
						<p class="section-about__info f12">
							W&nbsp;posiadaniu ponad 200 patentów oraz obecność produktów na&nbsp;rynkach w&nbsp;ponad
							70&nbsp;krajach. Autoryzowanyn dystrybutorem produktów w&nbsp;Polsce jest&nbsp;firma&nbsp;F.H. Knieja.
						</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<p class="text-center">
						<a class="button button-small" href="o-firmie" title="Produceny termowizjii - Guide Sensmart">
							<span class="button-text">
								O firmie
							</span>
							<span class="button-bg"></span>
						</a>
					</p>
				</div>
			</div>

		</section>
	</div>

</main>

<?php get_footer(); ?>