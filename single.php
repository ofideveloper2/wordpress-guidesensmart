<?php get_header(); ?>

<main role="main" class="page-content">

	<?php get_template_part('templates/template-slider'); ?>
	<div class="container ">
		<div class="d-back"> 
			
		<a href="/blog"><svg width="15" height="10" viewBox="0 0 11 7" version="1.1" xmlns="http://www.w3.org/2000/svg" style="transform: rotate(180deg);"> <path d="M0 2.926h8.258L6.18.82 7 0l3.5 3.5L7 7l-.82-.82 2.078-2.106H0z"></path> </svg></a>
				<a href="/blog" ><h3>  Powrót do bloga </h3> </a>
			
		</div> 
		
				<section class="wrapper" style=" border-bottom: 2px solid #d10000;">
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<h1 class="d-blog-title">
							<?php the_title(); ?>
						</h1>
						<div class="meta" style="margin-bottom: 1em;"> 
						<span> <?= get_the_author_meta('first_name');?>  | </span>
                		<span> <?= the_date('Y-m-d') ?> </span> 
						</div>
						

						
						
						<div class="page-wysiwig">
							<?php the_content(); ?>
						</div>

						<?php edit_post_link(); ?>
					</article>
				<?php endwhile; ?>
				<?php else: ?>
					<article>
						<p><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></p>
					</article>
				<?php endif; ?>
				</section>
		

			<div >
				<h3 class="d-blog-title-single" style="text-align: left;"> Najnowsze wpisy </h3>

				<?php
				 $d_args = array(
					'post_type' => 'd-post',
					'posts_per_page' => 3,
				
				);

				$d_posts = new WP_Query($d_args);
				?>

				<?php if($d_posts->have_posts()) : ?>
					<?php while($d_posts->have_posts()) : $d_posts->the_post(); ?>
						<div style="margin-bottom: 10px;">
							<div class="d-sitebar">
								   <a href="<?php the_permalink(); ?>"> <h3 style="margin: 0.5em 0; font-size: 1.5em;">  <?php the_title(); ?> </h3> </a>
								
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?> 
			</div>
		
	</div> 
</main>


<?php get_footer(); ?>
