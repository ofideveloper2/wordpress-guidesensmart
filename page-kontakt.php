<?php 

/*
	Template Name: Contact
*/

?>

<?php get_header(); ?>

<?php get_template_part('templates/template-breadcrumbs'); ?>

<main role="main" class="page-content">
	<section class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="page-title">
				  <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( '%s', 'twentyten' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
				</h1>
			</div>
		</div>

		<div class="row">
			<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<div class="col-6">
				<article id="post-<?php the_ID(); ?>">
					<div class="page-wysiwig">
						<div class="page-wysywig-spacer"></div>
						<?php the_content(); ?>
					</div>
				
					<?php edit_post_link('Edytuj stronę'); ?>
				</article>
			</div>
			<?php endwhile; ?>
			<?php endif; ?>
			<!-- 
			<div class="col-6">
					<div class="section-map">
						<div class="contact-map" id="map"></div>

						<a class="button button-small button-grey" href="https://www.google.com/maps/dir//ESTECARE+Klinika+Zdrowej+Sk%C3%B3ry+i+Medycyny+Estetycznej+26-600+Radom/@51.3972054,21.1291896,13z/data=!4m8!4m7!1m0!1m5!1m1!1s0x47185ec42f5ba611:0xf5549a05568a53d!2m2!1d21.1642947!2d51.3972106" target="_blank" title="Otwórz w google maps">
							<span class="button-text">
								Wskazówki dojazdu
							</span>
							<span class="button-bg"></span>
						</a>
					</div>

					<script>
						function initMap() {

							var styles = [
							{
								"elementType": "geometry",
								"stylers": [
								{
									"color": "#f5f5f5"
								}
								]
							},
							{
								"elementType": "labels.icon",
								"stylers": [
								{
									"visibility": "off"
								}
								]
							},
							{
								"elementType": "labels.text.fill",
								"stylers": [
								{
									"color": "#616161"
								}
								]
							},
							{
								"elementType": "labels.text.stroke",
								"stylers": [
								{
									"color": "#f5f5f5"
								}
								]
							},
							{
								"featureType": "administrative.land_parcel",
								"elementType": "labels.text.fill",
								"stylers": [
								{
									"color": "#bdbdbd"
								}
								]
							},
							{
								"featureType": "poi",
								"elementType": "geometry",
								"stylers": [
								{
									"color": "#eeeeee"
								}
								]
							},
							{
								"featureType": "poi",
								"elementType": "labels.text.fill",
								"stylers": [
								{
									"color": "#757575"
								}
								]
							},
							{
								"featureType": "poi.park",
								"elementType": "geometry",
								"stylers": [
								{
									"color": "#e5e5e5"
								}
								]
							},
							{
								"featureType": "poi.park",
								"elementType": "labels.text.fill",
								"stylers": [
								{
									"color": "#9e9e9e"
								}
								]
							},
							{
								"featureType": "road",
								"elementType": "geometry",
								"stylers": [
								{
									"color": "#ffffff"
								}
								]
							},
							{
								"featureType": "road.arterial",
								"elementType": "labels.text.fill",
								"stylers": [
								{
									"color": "#757575"
								}
								]
							},
							{
								"featureType": "road.highway",
								"elementType": "geometry",
								"stylers": [
								{
									"color": "#dadada"
								}
								]
							},
							{
								"featureType": "road.highway",
								"elementType": "labels.text.fill",
								"stylers": [
								{
									"color": "#616161"
								}
								]
							},
							{
								"featureType": "road.local",
								"elementType": "labels.text.fill",
								"stylers": [
								{
									"color": "#9e9e9e"
								}
								]
							},
							{
								"featureType": "transit.line",
								"elementType": "geometry",
								"stylers": [
								{
									"color": "#e5e5e5"
								}
								]
							},
							{
								"featureType": "transit.station",
								"elementType": "geometry",
								"stylers": [
								{
									"color": "#eeeeee"
								}
								]
							},
							{
								"featureType": "water",
								"elementType": "geometry",
								"stylers": [
								{
									"color": "#c9c9c9"
								}
								]
							},
							{
								"featureType": "water",
								"elementType": "labels.text.fill",
								"stylers": [
								{
									"color": "#9e9e9e"
								}
								]
							}
							];

							var center = {
								lat: 51.396680, 
								lng: 21.163990
							}

							// Create a map object and specify the DOM element for display.
							this.map = new google.maps.Map(document.getElementById('map'), {
								center: center,
								scrollwheel: false,
								mapTypeControl: true, // false
								zoom: 6,
								streetViewControl: false,
								mapTypeControl: false,
								disableDefaultUI: false // true
							});

							if (window.innerWidth < 768) {
								this.map.setCenter({
									lat: 51.396680, 
									lng: 21.163990
								})
							};

							var mapImage = {
							    url: '<?php echo get_template_directory_uri(); ?>'+'/assets/img/google-maps-marker.png'
							  };

							// var icon = "/google-maps-marker.png";
							var marker = new google.maps.Marker({
								map: this.map,

								icon: mapImage,
								position: {lat: 51.396680, lng: 21.163990}
							});
						}
					</script>
					<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxIauAclNMt92h1U0FdmlJFN30rMAdYIg&callback=initMap" async defer></script>
			</div>
			 -->
		</div>

		<!-- <div class="page-form" id="formularz-kontaktowy">
			<form id="contact" action="<?php echo site_url(); ?>/form_contact_submit.php">
				<div class="row">
					<div class="col-12">
						<h3 class="page-form-title mb-0">
							Formularz kontaktowy
						</h3>
						<p class="mt-0 mb-0">Wszystkie pola są wymagane.</p>
					</div>
				</div>

				<div class="form-body">
					<div class="row">
						<div class="col-6">
							<label for="name">Imię:</label>
							<input required type="text" name="participantname" id="name" placeholder="">
						</div>
						<div class="col-6">
							<label for="surname">Nazwisko:</label>
							<input required type="text" name="surname" id="surname" placeholder="">
						</div>
					</div>
					
					<div class="row">
						<div class="col-6">
								<label for="phone">Telefon:</label>
								<input required type="tel" name="phone" id="phone" placeholder="">
						</div>
						<div class="col-6">
							<label for="email">E-mail:</label>
							<input required type="email" name="email" id="email" placeholder="">
						</div>
					</div>

					<div class="row">
						<div class="col-12">
								<label for="message">Treść wiadomości:</label>
								<textarea name="message" id="message" cols="30" rows="6"></textarea>
						</div>
					</div>
					
					<div class="row">
						<div class="col-6">
							<div class="row-checkboxOuter">
								<label for="giodo">
									(*) Wyrażam zgodę na przetwarzanie danych osobowych w celu kontaktu. 
									Przed wyrażeniem zgody zapoznaj się z Polityką prywatności / Klauzulą informacyjną.
								</label>
								<input type="checkbox" required id="giodo" name="giodo">
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12">
						<div class="captchaHolder">
							<div data-sitekey="6Lc2JLkUAAAAAAeBSzUvX_7l-N4BMrFQQaBVTSVE" data-callback="captchaLoadCallback" class="g-recaptcha"></div>

							<div class="captchaHolder-error hidden">
								Pole wymagane
							</div>
						</div>
					</div>
				</div>

				<div class="row hidden row-message">
					<div class="col-12">
						<div class="error hidden">Wystąpił błąd, proszę uzupełnić wszystkie dane.</div>
						<div class="success hidden">Dziękuję {name} za wypełnienie formularza. Dane zostały wysłane.</div>
					</div>
				</div>

				<div class="row row-submit form-body">
					<div class="col-12">
						<button class="button button-small" href="#" type="submit">
							<span class="button-text">
								Wyślij
							</span>
							<span class="button-bg"></span>
						</button>
					</div>
				</div>

				<div class="row row-preloader hidden">
					<div class="col-12">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/preloader.gif" alt="Wysyłam...">
					</div>
				</div>
			</form>
		</div> -->
		
	</section>
</main>

<?php get_footer(); ?>