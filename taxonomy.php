<?php get_header(); ?>

<?php 

$current_term = get_queried_object();

$paged = (get_query_var( 'page' )) ? (get_query_var('page')) : 1;

$args = array(
	'orderby' => 'date',
    'order'   => 'DESC',
	'post_type'        => 'product',
	'paged'			   => $paged,
	'post_status'      => 'publish',
	'suppress_filters' => true,
	'posts_per_page'	=> 100,
	'tax_query' => array(
		array(
			'taxonomy' => $current_term->taxonomy, // you can change it according to your taxonomy
			'field' => 'term_id', // this can be 'term_id', 'slug' & 'name'
			'terms' => $current_term->term_id,
		)
	)
);

$the_query = new WP_Query($args);

?>

<?php get_template_part('templates/template-breadcrumbs'); ?>

<main role="main" class="page-content">
	<section class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="page-title">
                	<a href="<?php echo get_term_link($current_term->term_id); ?>" title="<?php echo $current_term->name; ?>">
						<?php echo $current_term->name; ?>
					</a>
                </h1>

				<?php if ($current_term->description) ?>
				<div class="row">
					<div class="col-md-6">
						<p>
							<?php echo $current_term->description; ?>
						</p>
					</div>
				</div>
				<?php ?>
			</div>
		</div>

		<?php hm_get_template_part('templates/template-products-all', [
			'query_arr' => $args
		]); ?>

	</section>
</main>

<?php get_footer(); ?>