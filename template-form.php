<div class="page-form" id="formularz-kontaktowy">
	<form id="request" action="<?php echo site_url(); ?>/form_request_submit.php">
        <div class="row">
            <div class="col-12">
                <div class="title-borderOuter">
                    <h2 class="title-border">Zapytaj o&nbsp;indywidualną ofertę</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 offset-md-3">
                <p class="text-center mt-0">
                    Pozostaw dane kontaktowe, a skontaktujemy się z&nbsp;Państwem celem przedstawienia indywidualnej oferty.
                </p>
            </div>
        </div>

		<div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="form-body">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-row">
                                <label for="name">Imię:</label>
                                <input required type="text" name="name" id="name" placeholder="">
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-row">
                                <label for="phone">Telefon / E-mail:</label>
                                <input required type="tel" name="phone" id="phone" placeholder="">
                            </div>
                        </div>
                        
                        <!-- <div class="col-6">
                            <div class="form-row">
                                <label for="surname">Nazwisko:</label>
                                <input required type="text" name="surname" id="surname" placeholder="">
                            </div>
                        </div> -->
                    </div>
                    
                    <div class="row">
                        <div class="col-6">
                            
                        </div>
                        <!-- <div class="col-6">
                            <div class="form-row">
                                <label for="email">E-mail:</label>
                                <input required type="email" name="email" id="email" placeholder="">
                            </div>
                        </div> -->
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-row">
                                <label for="message">Wiadomość:</label>
                                <textarea cols="30" rows="10" id="message" name="message">
Dzień dobry,

poproszę o indywidualną ofertę na "<?php echo $template_args['message']; ?>".

</textarea>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-12">
                            <div class="form-row">
                                <div class="row-checkboxOuter">
                                    <label for="giodo">
                                        (*) Wyrażam zgodę na przetwarzanie danych osobowych w celu kontaktu. Oświadczam, że zapoznałam/em się z&nbsp;polityką prywatności.
                                    </label>
                                    <input type="checkbox" required id="giodo" name="giodo">
                                </div>
                            </div>
                        </div>
                    </div>
        
                    <div class="row">
                        <div class="col-12">
                            <div class="form-row mb-0">
                                <div class="captchaHolder">
                                    <div data-sitekey="6Lf5FPAUAAAAADA_lCA62Hyqzivm5km2fhl6W6qh" data-callback="captchaLoadCallback" class="g-recaptcha"></div>
                                    
                                    <div class="captchaHolder-error hidden">
                                        Pole wymagane
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        
                <div class="row hidden row-message">
                    <div class="col-12">
                        <div class="error hidden">Wystąpił błąd, proszę uzupełnić wszystkie dane.</div>
                        <div class="success hidden">
                            {name}, dziękujemy za wypełnienie formularza. Dane zostały wysłane. <br>
                            Wkrótce skontaktujemy się celem przedstawienia oferty.</div>
                    </div>
                </div>
        
                <div class="row row-submit form-body">
                    <div class="col-12">
                        <button class="button button-small" href="#" type="submit">
                            <span class="button-text">
                                Wyślij
                            </span>
                            <span class="button-bg"></span>
                        </button>
                    </div>
                </div>
        
                <div class="row row-preloader hidden">
                    <div class="col-12">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/preloader.gif" alt="Wysyłam...">
                    </div>
                </div>
            </div>
        </div>
	</form>
</div>