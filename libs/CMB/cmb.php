<?php

if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
}

if ( file_exists( dirname( __FILE__ ) . '/cmb2-taxonomy/init.php' ) ) {
    require_once dirname( __FILE__ ) . '/cmb2-taxonomy/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/cmb2-taxonomy/init.php' ) ) {
    require_once dirname( __FILE__ ) . '/cmb2-taxonomy/init.php';
}

require_once dirname( __FILE__ ) . '/theme-options.php';

add_filter('wp_nav_menu_objects','mrw_tax_archive_current');
add_action( 'init', 'revcon_change_page_object' );
add_action( 'init', 'products_post_type');
add_action( 'cmb2_admin_init', 'product_metabox' );
add_action( 'cmb2_admin_init', 'homepage_metabox' );
add_action( 'cmb2_admin_init', 'guarantee_metabox' );
add_action( 'cmb2_admin_init', 'shops_metabox' );

register_taxonomy( 'type', 
	array('product'),
	array('hierarchical' => true,    
		'labels' => array(
			'name' => __( 'Typ, kategoria produktu' ),
			'singular_name' => __( 'Typ, kategoria produktu' ),
			'search_items' =>  __( 'Wyszukaj' ),
			'all_items' => __( 'Wszystkie' ),
			'parent_item' => __( 'Nadrzędny' ),
			'parent_item_colon' => __( 'Nadrzędny' ),
			'edit_item' => __( 'Edytuj' ),
			'update_item' => __( 'Aktualizuj' ),
			'add_new_item' => __( 'Dodaj nowy' ),
			'new_item_name' => __( 'Zaktualizuj' ),
			'menu_name' => __( 'Typ, kategoria produktu' )
        ),
		'show_admin_column' => true, 
        'show_in_nav_menus' => true,
        'rewrite'           =>  array('slug' => 'cats', 'with_front' => true),
	)
);

register_taxonomy( 'series', 
	array('product'),
	array('hierarchical' => true,    
		'labels' => array(
			'name' => __( 'Seria produktu' ),
			'singular_name' => __( 'Seria produktu' ),
			'search_items' =>  __( 'Wyszukaj' ),
			'all_items' => __( 'Wszystkie' ),
			'parent_item' => __( 'Nadrzędny' ),
			'parent_item_colon' => __( 'Nadrzędny' ),
			'edit_item' => __( 'Edytuj' ),
			'update_item' => __( 'Aktualizuj' ),
			'add_new_item' => __( 'Dodaj nowy' ),
			'new_item_name' => __( 'Zaktualizuj' ),
			'menu_name' => __( 'Seria produktu' )
        ),
		'show_admin_column' => true, 
        'show_ui' => true,
        'show_in_quick_edit' => false,
        'meta_box_cb'                => false,
        'query_var' => false,
        'rewrite'           =>  array('slug' => 'series', 'with_front' => true),
	
	)
);

function products_post_type() { 
    register_post_type( 'product', 
        array( 'labels' => array(
                'name'              => __( 'Produkty' ),
                'singular_name'     => __( 'Produkty' ),
                'all_items'         => __( 'Wszystkie produkty' ),
                'add_new'           => __( 'Dodaj nowy produkt' ),
                'add_new_item'      => __( 'Dodaj' ),
                'edit'              => __( 'Edytuj' ),
                'edit_item'         => __( 'Edytuj wybrany produkt' ),
                'new_item'          => __( 'Dodaj' ),
                'view_item'         => __( 'Wyświetl' ),
                'search_items'      => __( 'Wyszukaj' ),
                'not_found'         =>  __( 'Nie znaleziono w bazie danych' ),
                'not_found_in_trash' => __( 'Nie znaleziono w koszu' ),
                'parent_item_colon' => ''
            ),
            'description'           => __( 'Produkty' ),
            'public'                => true,
            'publicly_queryable'    => true,
            'exclude_from_search'   => false,
            'show_ui'               => true,
            'query_var'             => true,
            'show_in_nav_menus'     => true,
            'menu_position'         => 3,
            'menu_icon'             => 'dashicons-feedback',
            'hierarchical'          => true,
            'taxonomies'            => array('type', 'series'),
            'supports'              => array('title')
           
        ) 
    ); 
}

function shops_metabox() {
    $prefix = 'shops_';

    $cmb = new_cmb2_box( array(
        'id'            => $prefix . 'metabox',
        'title'         => esc_html__( 'Podstawowe informacje', 'cmb2' ),
        'object_types'  => array( 'dystrybutorzy' ),
        'context'    => 'advanced',
        'priority'   => 'core',
    ));

    $cmb->add_field( array(
        'name' => 'Tytuł',
        'id'   => $prefix . 'title',
        'type' => 'text'
    ));

    $cmb->add_field( array(
        'name'           => 'Ulica',
        'id'             => $prefix . 'street',
        'type'           => 'text',
    ));

    $cmb->add_field( array(
        'name'           => 'Kod pocztowy',
        'id'             => $prefix . 'zipcode',
        'type'           => 'text',
    ));
    
    $cmb->add_field( array(
        'name'           => 'Miasto / miejscowość',
        'id'             => $prefix . 'city',
        'taxonomy'       => 'miasto',
        'type'           => 'text',
    ));

    $cmb->add_field( array(
        'name'           => 'E-mail',
        'id'             => $prefix . 'email',
        'type'           => 'text_email',
    ));

    $cmb->add_field( array(
        'name'           => 'Telefon',
        'id'             => $prefix . 'phone',
        'type'           => 'text',
    ));

    $cmb->add_field( array(
        'name'           => 'Telefon (alternatywny)',
        'id'             => $prefix . 'phone2',
        'type'           => 'text',
    ));


    $cmb->add_field( array(
        'name'           => 'WWW',
        'id'             => $prefix . 'www',
        'type'           => 'text_url',
    ));

    $cmb->add_field( array(
        'name'           => 'Województwo',
        'id'             => $prefix . 'region',
        'taxonomy'       => 'wojewodztwo',
        'type'           => 'taxonomy_select',
       
        'text'           => array(
            'no_terms_text' => 'Nic nie znaleziono'
        ),
        'remove_default' => 'true',
        'query_args' => array(
            'orderby' => 'slug',
           
        ),
    ));

    $cmb->add_field( array(
        'name' => 'Szerokośc geograficzna (latitude)',
        'id'   => $prefix . 'lat',
        'type' => 'text'
    ));

    $cmb->add_field( array(
        'name' => 'Długość geograficzna (longitude)',
        'id'   => $prefix . 'lng',
        'type' => 'text'
    ));

    $cmb->add_field( array(
        'name'    => 'Obrazek',
        'desc'    => 'Wybierz.',
        'id'      => $prefix.'image',
        'type'    => 'file',
       
        'options' => array(
            'url' => false,
        ),
        'text'    => array(
            'add_upload_file_text' => 'Dodaj'
        ),
       
        'query_args' => array(
           
           
            'type' => array(
            
            	'image/jpeg',
            	'image/png',
            ),
        ),
        'preview_size' => 'large',
    ));
}

function product_metabox() {
    $prefix = 'product_';

    $cmb = new_cmb2_box( array(
        'id'            => $prefix . 'metabox',
        'title'         => esc_html__( 'Podstawowe informacje', 'cmb2' ),
        'object_types'  => array( 'product' ),
       
        'context'    => 'advanced',
        'priority'   => 'core',
    ));

    $cmb->add_field( array(
        'name' => 'Tytuł produktu (listing)',
        'id'   => $prefix . 'title',
        'type' => 'text'
    ));

    $cmb->add_field( array(
        'name' => 'Tytuł po angielsku (dla SEO)',
        'id'   => $prefix . 'title_en',
        'type' => 'text'
    ));

    $cmb->add_field( array(
        'name' => 'Cena',
        'id'   => $prefix . 'price',
        'type' => 'text'
    ));

    $cmb->add_field( array(
        'name'           => 'Wybierz kategorię produktu',
        'id'             => $prefix . 'type',
        'taxonomy'       => 'type', 
        'type'           => 'taxonomy_select',
       
        'text'           => array(
            'no_terms_text' => 'Nic nie znaleziono'
        ),
        'remove_default' => 'true',
       
        'query_args' => array(
            'orderby' => 'slug',
           
        ),
    ) );
    
    $cmb->add_field( array(
        'name'           => 'Wybierz serię',
        'id'             => $prefix . 'series',
        'taxonomy'       => 'series',
        'type'           => 'taxonomy_select',
       
        'text'           => array(
            'no_terms_text' => 'Nic nie znaleziono'
        ),
        'remove_default' => 'true',
        'query_args' => array(
            'orderby' => 'slug',
           
        ),
    ));

    $cmb->add_field( array(
        'name'    => 'Obrazek (listing)',
        'desc'    => 'Wybierz.',
        'id'      => $prefix.'image',
        'type'    => 'file',
       
        'options' => array(
            'url' => false,
        ),
        'text'    => array(
            'add_upload_file_text' => 'Dodaj'
        ),
       
        'query_args' => array(
            'type' => array(
            
            	'image/jpeg',
            	'image/png',
            ),
        ),
        'preview_size' => 'large',
    ));


    $cmb->add_field( array(
        'name'    => 'Galeria',
        'desc'    => 'Wybierz.',
        'id'      => $prefix.'image_gallery',
        'type'    => 'file_list',
       
        'options' => array(
            'url' => false,
        ),
        'text'    => array(
            'add_upload_file_text' => 'Dodaj'
        ),
       
        'query_args' => array(
            'type' => array(
            
            	'image/jpeg',
            	'image/png',
            ),
        ),
        'preview_size' => 'small',
    ));
    
    $cmb->add_field( array(
        'name'    => 'Krótki opis',
        'id'      => $prefix.'short_desc',
        'type'    => 'wysiwyg',
        'options' => array(
            'wpautop' => true,
            'media_buttons' => false,
            'textarea_rows' => get_option('default_post_edit_rows', 5),
            'editor_css' => '',
            'editor_class' => '',
            'teeny' => false,
            'dfw' => false,
            'tinymce' => true,
            'quicktags' => false
        ),
    ));

    $cmb->add_field( array(
        'name'    => 'Broszura PDF',
        'desc'    => 'Wybierz.',
        'id'      => $prefix.'brouche',
        'type'    => 'file',
       
        'options' => array(
            'url' => false,
        ),
        'text'    => array(
            'add_upload_file_text' => 'Wybierz / wgraj'
        ),
       
        'query_args' => array(
            'type' => 'application/pdf',
        ),
        'preview_size' => 'small',
    ));

    $cmb->add_field( array(
        'name' => 'Link przycisku <br/>Kup teraz',
        'id'   => $prefix . 'buy',
        'type' => 'text_url'
    ));

    $cmb = new_cmb2_box( array(
        'id'            => $prefix . 'benefits',
        'title'         => esc_html__( 'Zalety', 'cmb2' ),
        'object_types'  => array( 'product' ),
        'context'    => 'advanced',
        'priority'   => 'core',
        'closed'     => false,
    ));

    $cmb->add_field( array(
        'name' => esc_html__( 'Dodaj bloki z zaletami produktu', 'cmb2' ),
        'id'   => 'title',
        'type' => 'title',
    ));

    $benefits = $cmb->add_field( array(
        'id'          => $prefix . 'benefits',
        'type'        => 'group',
        'options'     => array(
            'group_title'   => esc_html__( 'Bloki z zaletą - {#}', 'cmb2' ), 
            'add_button'    => esc_html__( 'Dodaj kolejny', 'cmb2' ),
            'remove_button' => esc_html__( 'Usuń', 'cmb2' ),
            'sortable'      => true, 
            'closed'     => false,
        ),
    ));

    $cmb->add_group_field( $benefits, array(
        'name'       => esc_html__( 'Tekst', 'cmb2' ),
        'id'         => 'content',
        'type'    => 'wysiwyg',
        'sanitization_cb' => false,
        'options' => array(
            'wpautop' => false,
            'media_buttons' => true,
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'editor_css' => '',
            'editor_class' => '',
            'teeny' => true,
            'dfw' => false,
            'tinymce' => true,
            'quicktags' => true
        ),
    ));

    $cmb->add_group_field( $benefits, array(
        'name'    => 'Obrazek',
        'id'      => 'image',
        'type'    => 'file',
       
        'options' => array(
            'url' => true,
        ),
        'text'    => array(
            'add_upload_file_text' => 'Wybierz'
        ),
       
        'query_args' => array(
           
           
            'type' => array(
            
            	'image/jpeg',
            	'image/png',
            ),
        ),
        'preview_size' => 'small',
    ));

    $cmb = new_cmb2_box( array(
        'id'            => $prefix . 'params',
        'title'         => esc_html__( 'Parametry', 'cmb2' ),
        'object_types'  => array( 'product' ),
        'context'    => 'advanced',
        'priority'   => 'core',
        'closed'     => true,
    ));

    $cmb->add_field( array(
        'name' => 'Przypinaj modele przy scrollowaniu?',
        'id'   => $prefix . 'params_sticky',
        'type'    => 'checkbox',
        'default' => false
    ));

    $cmb->add_field( array(
        'name' => '',
        'id'   => $prefix . 'params_content',
        'type'    => 'wysiwyg',
        'options' => array(
            'wpautop' => true,
            // 'media_buttons' => false,
            'textarea_rows' => get_option('default_post_edit_rows', 15),
            // 'editor_css' => '',
            // 'editor_class' => '',
            'teeny' => false,
            'dfw' => true,
            // // 'tinymce' => true,
            // // 'quicktags' => false
        ),
    ));
}

function homepage_metabox() {
    $prefix = 'homepage_';

    $cmb = new_cmb2_box( array(
        'id'            => $prefix . 'metabox',
        'title'         => esc_html__( 'Slider', 'cmb2' ),
        'object_types'  => array( 'page' ),
        'show_on'      => array( 'key' => 'page-template', 'value' => 'page-homepage.php' ),
       
        'context'    => 'advanced',
        'priority'   => 'core',
    ));

    $cmb->add_field( array(
        'name' => esc_html__( 'Ustawienia slidera', 'cmb2' ),
        'desc' => esc_html__( 'Ustaw elementy, które będą się wyświetlać nad treścią strony. Jeśli czegoś ma nie być wystarczy zostawić puste pole.', 'cmb2' ),
        'id'   => 'title',
        'type' => 'title',
    ) );

    $slider = $cmb->add_field( array(
        'id'          => $prefix . 'slider',
        'type'        => 'group',
        'options'     => array(
            'group_title'   => esc_html__( 'Slider - {#}', 'cmb2' ), 
            'add_button'    => esc_html__( 'Dodaj kolejny', 'cmb2' ),
            'remove_button' => esc_html__( 'Usuń', 'cmb2' ),
            'sortable'      => true, 
            'closed'     => true,
        ),
    ));

    $cmb->add_group_field( $slider, array(
        'name'       => esc_html__( 'Główny tytuł', 'cmb2' ),
        'id'         => 'title',
        'type'       => 'text',
    ) );

    $cmb->add_group_field( $slider, array(
        'name'       => esc_html__( 'Podtytuł', 'cmb2' ),
        'id'         => 'desc',
        'type'       => 'text',
    ));

    $cmb->add_group_field( $slider, array(
        'name'       => esc_html__( 'Tekst na przycisku', 'cmb2' ),
        'id'         => 'button_copy',
        'type'       => 'text',
        'default'	=> 'Sprawdź'
       
    ));

    $cmb->add_group_field( $slider, array(
        'name' => esc_html__( 'Gdzie kieruje link?', 'cmb2' ),
        'desc' => esc_html__( 'Podaj poprawy adres do strony.', 'cmb2' ),
        'id'   => 'button_url',
        'type' => 'text_url',
    ) );

    $cmb->add_group_field( $slider, array(
        'name' => esc_html__( 'Obrazek', 'cmb2' ),
        'desc' => esc_html__( 'Wgraj obrazek, lub wprowadź adres URL. ', 'cmb2' ),
        'id'   => 'image',
        'type' => 'file',
        'text'    => array(
            'add_upload_file_text' => 'Wgraj / Wybierz'
        ),
    ));

    $cmb->add_group_field( $slider, array(
        'name' => esc_html__( 'Obrazek - telefon - portretowy', 'cmb2' ),
        'desc' => esc_html__( 'Wgraj obrazek, lub wprowadź adres URL.', 'cmb2' ),
        'id'   => 'image_mobile',
        'type' => 'file',
        'text'    => array(
            'add_upload_file_text' => 'Wgraj / Wybierz'
        ),
    ));
}


add_action( 'init', 'd_post_type');


function d_post_type() {
    register_post_type( 'd-post',
    array(
        'labels' => array(
            'name' => __( 'Blog' ),
            'singular_name' => __( 'Blog' )
        ),
        'public' => true,
        'publicly_queryable' => true,   
        'capability_type' => 'page',
        'has_archive' => false,
        'rewrite' => array('slug' => 'blog'),
        'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'page-attributes' )
    )
);
}









add_action( 'init', 'shop_post_type');

function shop_post_type() {
    register_post_type( 'dystrybutorzy', 
        array( 'labels' => array(
                'name'              => __( 'Dystrybutorzy' ),
                'singular_name'     => __( 'Dystrybutorzy' ),
                'all_items'         => __( 'Wszyscy dystrybutorzy' ),
                'add_new'           => __( 'Dodaj nowego' ),
                'add_new_item'      => __( 'Dodaj' ),
                'edit'              => __( 'Edytuj' ),
                'edit_item'         => __( 'Edytuj' ),
                'new_item'          => __( 'Dodaj' ),
                'view_item'         => __( 'Wyświetl' ),
                'search_items'      => __( 'Wyszukaj' ),
                'not_found'         =>  __( 'Nie znaleziono w bazie danych' ),
                'not_found_in_trash' => __( 'Nie znaleziono w koszu' ),
                'parent_item_colon' => ''
            ),
            'description'           => __( 'Dystrybutorzy' ),
            'public'                => true,
            'publicly_queryable'    => true,
            'exclude_from_search'   => false,
            'show_ui'               => true,
            'query_var'             => true,
            'show_in_nav_menus'     => true,
            'menu_position'         => 3,
            'menu_icon'             => 'dashicons-feedback',
            'hierarchical'          => true,
            'supports'              => array('title')
           
        ) 
    );
}

register_taxonomy( 'wojewodztwo', 
	array('dystrybutorzy'),
	array('hierarchical' => true,
		'labels' => array(
			'name' => __( 'Województwa' ),
			'singular_name' => __( 'Województwa' ),
			'search_items' =>  __( 'Wyszukaj' ),
			'all_items' => __( 'Wszystkie' ),
			'parent_item' => __( 'Nadrzędny' ),
			'parent_item_colon' => __( 'Nadrzędny' ),
			'edit_item' => __( 'Edytuj' ),
			'update_item' => __( 'Aktualizuj' ),
			'add_new_item' => __( 'Dodaj nowy' ),
			'new_item_name' => __( 'Zaktualizuj' ),
			'menu_name' => __( 'Województwa' )
        ),
		'show_admin_column' => true, 
        'show_in_nav_menus' => true,
        'rewrite'           =>  array('slug' => 'wojewodztwo', 'with_front' => true),
	)
);

function guarantee_metabox() {
    $prefix = 'guarantee_';
    
    $cmb = new_cmb2_box( array(
        'id'            => $prefix . 'metabox',
        'title'         => esc_html__( 'Numery seryjne', 'cmb2' ),
        'object_types'  => array( 'page' ),
        'show_on'      => array( 'key' => 'page-template', 'value' => 'page-gwarancja.php' ),
       
        'context'    => 'advanced',
        'priority'   => 'core',
    ));

    $cmb->add_field( array(
        'id'   => $prefix . 'skus',
        'type' => 'textarea_code'
    ));
}

add_filter('cmb2-taxonomy_meta_boxes', 'type_metaboxes');

function type_metaboxes( array $meta_boxes ) {
    $prefix = 'type_';
    $meta_boxes['offer_metaboxes'] = array(
        'id'            => 'offer_metaboxes',
        'title'         => __( 'Dodatkowe informacje', 'cmb2' ),
        'object_types'  => array( 'type' ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, 
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        'fields'        => array(
            array(
                'name' => __( 'Obrazek kategorii (pokazywany na strone głównej)', 'cmb2' ),
                'desc' => __( 'Wgraj obrazek lub podaj adres URL obrazka.', 'cmb2' ),
                'id'   => $prefix . 'image',
                'type' => 'file',
                'text'    => array(
                    'add_upload_file_text' => 'Wgraj / wybierz zdjęcie' // Change upload button text. Default: "Add or Upload File"
                ),
            ),
        ),
    );

    return $meta_boxes;
}

function mrw_tax_archive_current( $items ) {
    foreach ( $items as $item ) {
        if ( 'taxonomy' !== $item->type )
            continue;

        global $post;

        if(!$post)
            continue;

        $taxonomy = $item->object;
        $taxonomy_term = $item->object_id;
        if (
            ! is_tax( $taxonomy, $taxonomy_term )
            AND ! has_term( $taxonomy_term, $taxonomy, $post->ID )
        )
            continue;

       
        $item->current = true;
        $item->classes[] = 'current-menu-item';

       
        $active_anc_item_ids = mrw_get_item_ancestors( $item );
        foreach ( $items as $key => $parent_item ) {
            $classes = (array) $parent_item->classes;

           
            if ( $parent_item->db_id == $item->menu_item_parent ) {
                $classes[] = 'current-menu-parent';
                $items[ $key ]->current_item_parent = true;
            }

           
            if ( in_array( intval( $parent_item->db_id ), $active_anc_item_ids ) ) {
                $classes[] = 'current-menu-ancestor';
                $items[ $key ]->current_item_ancestor = true;
            }

            $items[ $key ]->classes = array_unique( $classes );
        }
    }

    return $items;
}

function mrw_get_item_ancestors( $item ) {
    $anc_id = absint( $item->db_id );

    $active_anc_item_ids = array();
    while (
        $anc_id = get_post_meta( $anc_id, '_menu_item_menu_item_parent', true )
        AND ! in_array( $anc_id, $active_anc_item_ids )
    )
        $active_anc_item_ids[] = $anc_id;

    return $active_anc_item_ids;
}

function hide_if_no_cats( $field ) {
	if ( ! has_tag( 'cats', $field->object_id ) ) {
		return false;
	}
	return true;
}

function show_if_front_page( $cmb ) {
	if ( get_option( 'page_on_front' ) !== $cmb->object_id ) {
		return false;
	}
	
	return true;
}

function revcon_change_page_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['page']->labels;
    $labels->name = 'Strony statyczne';
    $labels->singular_name = 'Strony statyczne';
    $labels->menu_name = 'Strony statyczne';
    $labels->name_admin_bar = 'Stronę statyczną';
    $menu_position = &$wp_post_types['page']->menu_position;
    $menu_position = 2;
}

function custom_taxonomy_flush_rewrite() {
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}

add_action('init', 'custom_taxonomy_flush_rewrite');