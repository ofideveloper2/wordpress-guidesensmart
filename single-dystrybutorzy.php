<?php get_header(); ?>

<?php get_template_part('templates/template-breadcrumbs'); ?>

<main role="main" class="page-content">
	<section class="container">
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <?php
                $postID = get_the_ID();
                $shop_name = get_post_meta($postID, 'shops_title', true);

                $shops_street = get_post_meta($postID, 'shops_street', true);
                $shops_zipcode = get_post_meta($postID, 'shops_zipcode', true);
                $shops_city = get_post_meta($postID, 'shops_city', true);

                $shops_www = get_post_meta($postID, 'shops_www', true);
                $shops_phone = get_post_meta($postID, 'shops_phone', true);
                $shops_phone2 = get_post_meta($postID, 'shops_phone2', true);
                $shops_email = get_post_meta($postID, 'shops_email', true);

                $shops_region = wp_get_post_terms($postID, 'wojewodztwo');
                $shops_region = $shops_region[0];
            ?>
        
            <div class="row">
                <div class="col-12">
                    <h2 class="page-title">
                        <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( '%s', 'twentyten' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark">
                            Termowizja dla myśliwych Guide Sensmart w mieście <br>
                            <?php echo $shops_city; ?>
                        </a>
                    </h2>

                    <h3 class="mb-0">
                        SKLEP: <strong><?php echo $shop_name; ?></strong>
                    </h3>
                    <p class="page-shops__preview-address">
                            <span class="d-block"><?php echo $shops_street; ?></span>
                            <span class="d-block"><?php echo $shops_zipcode; ?> <span><?php echo $shops_city; ?></span></span>
                        </p>

                        <p class="link-red">
                            <?php if ($shops_www) { ?>
                            <span class="d-block">WWW: <a target="_blank" href="<?php echo $shops_www; ?>/?utm_source=guideir.com.pl"><?php echo $shops_www; ?></a></span>
                            <?php } ?>
                            <?php if ($shops_email) { ?>
                            <span class="d-block">E-mail: <a href="mailto:<?php echo $shops_email; ?>?subject=Zapytanie o termowizję Guide Sensmart"><?php echo $shops_email; ?></a></span>
                            <?php } ?>
                            <?php if ($shops_phone) { ?>
                            <span class="d-block">
                                Telefon:
                                <a href="tel:<?php echo $shops_phone; ?>"><?php echo $shops_phone; ?></a>
                            </span>
                            <?php } ?>

                            <?php if ($shops_phone2) { ?>
                                <span class="d-block">
                                    Telefon:
                                    <a href="tel:<?php echo $shops_phone2; ?>"><?php echo $shops_phone2; ?></a>
                                </span>
                            <?php } ?>
                        </p>

                        <p>
                            <a href="https://www.google.pl/maps/search/<?php echo $title_converted; ?>+<?php echo $shops_city; ?>//data=" class="button button-small" target="_blank">
                                <span class="button-text">Wskazówki dojazdu &rarr;</span>
                                <span class="button-bg"></span>
                            </a>
                        </p>

                    <a href="/dystrybutorzy/" title="Termowizja Guide Sensmart - mapa dystrybutorów">
                        <span class="button-text">&larr; Inni dystrybutorzy</span>
                        <span class="button-bg"></span>
                    </a>
                </div>
            </div>
        
			<div class="row">
				<div class="col-12">
					<article id="post-<?php the_ID(); ?>">
						<div class="page-wysiwig">
							<div class="page-wysywig-spacer"></div>
							<?php the_content(); ?>
						</div>
					
						<?php edit_post_link('Edytuj stronę'); ?>
					</article>
				</div>
			</div>
		<?php endwhile; ?>
		<?php endif; ?>
	</section>
</main>

<?php get_footer(); ?>