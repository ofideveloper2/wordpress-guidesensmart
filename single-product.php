<?php get_header(); ?>

<?php get_template_part('templates/template-breadcrumbs'); ?>

<main role="main" class="page-content page-product">
	<article class="container">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<?php
				$postID = get_the_ID();

				$product_type = wp_get_post_terms($postID, 'type');
				$product_type = $product_type[0];

				$product_serie = wp_get_post_terms($postID, 'series');
				$product_series = $product_serie[0];

				$image = get_post_meta($postID, 'product_image', true);
				$gallery = get_post_meta($postID, 'product_image_gallery', true);
				$short_desc = get_post_meta($postID, 'product_short_desc', true);

				$brouche = get_post_meta($postID, 'product_brouche', true);
				$cta_buy = get_post_meta($postID, 'product_buy', true);
				$product_title_en = get_post_meta($postID, 'product_title_en', true);

				$product_benefits = get_post_meta($postID, 'product_benefits', true);
				$product_parameters = get_post_meta($postID, 'product_params_content', true);
				$product_parametersSticky = get_post_meta($postID, 'product_params_sticky', true);

				$product_price = get_post_meta($postID, 'product_price', true);
			?>

			
			<div class="product-popup">
				<div class="row">
					<div class="col-md-5 text-center">
						<?php if ($image) { ?>
							<img src="<?php echo $image; ?>" alt="<?php the_title(); ?> - termowizja sklep online Knieja">
						<?php } else { ?>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-no-image.png" alt="<?php echo $term->name; ?> - termowizja sklep online">
						<?php } ?>
					</div>
					<div class="col-md-7">
						<?php if ($cta_buy) { ?>
							<h3 class="text-center">Kup teraz:</h3>
							<a class="button button-small button-orange" target="_blank" href="<?php echo $cta_buy; ?>" title="Kup teraz">
								<span class="button-text">
									Online
								</span>
								<span class="button-bg"></span>
							</a>

							<?php if (!sensmart_get_option('is_camera')) { ?>
							<hr class="page-buyLocallyStrapline">
							<a class="button button-small button-blue" href="/dystrybutorzy" title="Kup lokalnie">
								<span class="button-text">
									Lokalnie
								</span>
								<span class="button-bg"></span>
							</a>
							<?php } ?>
						<?php } else if (sensmart_get_option('is_camera')) { ?>
							<a class="button button-small button-orange" target="_blank" href="#formularz-kontaktowy" title="Formularz kontaktowy">
								<span class="button-text">
									Zapytaj o&nbsp;ofertę
								</span>
								<span class="button-bg"></span>
							</a>
						<?php } ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">

					<div class="product-gallery-outer">
						<?php if ($gallery) { ?>
							<div class="product-gallery">
	
								<?php foreach($gallery as $_image) { ?>
									<div class="product-gallery-item">
										<div class="benefit-table">
											<div class="benefit-tableCell">
												<img src="<?php echo $_image; ?>" />
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						<?php } else { ?>
							<?php if ($image) { ?>
								<img src="<?php echo $image; ?>" alt="<?php the_title(); ?> - termowizja sklep online Knieja">
							<?php } else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-no-image.png" alt="<?php echo $term->name; ?> - termowizja sklep online">
							<?php } ?>
						<?php } ?>
					</div>
				</div>
				<div class="col-md-6">
					<!-- <a class="button-back" href="#">&larr; Produkty</a> -->
					
					<div class="product-info">
						<h1 class="mb-0">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_title(); ?>
							</a>
						</h1>
	
						<?php if ($product_title_en) { ?>
							<h3 class="mt-0">(<?php echo $product_title_en; ?>)</h3>
						<?php } ?>
	
						<?php if ($product_price) { ?>
						<p class="product-price">
							Cena: <?php echo $product_price; ?>
						</p>
						<?php } ?>
	
						<div class="page-product-buttons">
							<?php if ($cta_buy) { ?>
							<a class="button button-small button-orange" target="_blank" href="<?php echo $cta_buy; ?>?utm_source=guideir.com.pl" title="Kup teraz">
								<span class="button-text">
									Kup online
								</span>
								<span class="button-bg"></span>
							</a>
							<?php } else { ?>
							<a class="button button-small button-orange" target="_blank" href="#formularz-kontaktowy" title="Zapytaj o ofertę">
								<span class="button-text">
									Zapytaj o ofertę
								</span>
								<span class="button-bg"></span>
							</a>
							<?php } ?>
	
							<?php if ($brouche) { ?>
							<a class="button button-small" target="_blank" href="<?php echo $brouche; ?>" title="Pobierz broszurę (PDF)">
								<span class="button-text">
									Pobierz broszurę (PDF)
								</span>
								<span class="button-bg"></span>
							</a>
							<?php } ?>
	
							<?php if (!sensmart_get_option('is_camera')) { ?>
							<hr class="page-buyLocallyStrapline">
							<a class="button button-small button-blue" href="/dystrybutorzy" title="Kup lokalnie">
								<span class="button-text">
									Kup lokalnie
								</span>
								<span class="button-bg"></span>
							</a>
							<?php } ?>
						</div>
	
						<div class="page-wysiwig">
							<?php if ($product_type || $product_series) { ?>
								<p class="mt-0">
									Kategoria: <a title="<?php echo $product_type->name; ?>" href="<?php echo get_term_link($product_type); ?>"><?php echo $product_type->name; ?></a>
									| 
									<?php if ($product_series) { ?>
										Seria: <a title="<?php echo $product_series->name; ?>" href="<?php echo get_term_link($product_series); ?>"><?php echo $product_series->name; ?></a>
									<?php } ?>
								</p>
							<?php } ?>
							
							<?php if ($short_desc) { ?>
								<hr>
								<?php echo wpautop($short_desc); ?>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>

			<?php if ($product_benefits && $product_parameters) { ?>
			<div class="row">
				<div class="col-md-12">
					<ul class="page-tabNav">
						<?php if ($product_benefits) { ?><li><a href="#features" class="scrollTo">Zalety produktu</a></li><?php } ?>
						<?php if ($product_parameters) { ?><li><a href="#parameter" class="scrollTo" data-show="parameter">Parametry</a></li><?php } ?>
						<!-- <li><a href="#" data-show="application_of_the_image">Galeria</a></li>
						<li><a href="#" data-show="video">Wideo</a></li> -->
					</ul>
				</div>
			</div>

			<?php } ?>

			<div class="row">
				<div class="col-md-12">
					<?php if ($product_benefits) { ?>
					<?php $benefits_count = 0; ?>
					<div id="features" class="page-tabNavSection">
						<div class="row">
							<div class="col-12">
								<div class="title-borderOuter">
									<h2 class="title-border">Zalety produktu</h2>
								</div>
							</div>
						</div>
						
						<?php foreach ($product_benefits as &$benefit) { ?>
							<div class="row">
								<?php if ($benefit['content'] && $benefit['image']) { ?>
									<div class="col-md-6">
										<?php if ($benefits_count % 2) { ?>
											<div class="benefit-table">
												<div class="benefit-tableCell page-wysiwig">
													<img src="<?php echo $benefit['image']; ?>" alt="Img - <?php echo $benefits_count; ?>">	
												</div>
											</div>
										<?php } else { ?>
											<div class="benefit-table">
												<div class="benefit-tableCell page-wysiwig">
													<?php echo wpautop($benefit['content']); ?>
												</div>
											</div>
										<?php } ?>
									</div>

									<div class="col-lg-5 col-md-6 offset-lg-1">
										<?php if ($benefits_count % 2) { ?>
											<div class="benefit-table">
												<div class="benefit-tableCell page-wysiwig">
													<?php echo wpautop($benefit['content']); ?>
												</div>
											</div>
										<?php } else { ?>
											<div class="benefit-table">
												<div class="benefit-tableCell page-wysiwig">
													<img src="<?php echo $benefit['image']; ?>" alt="Img - <?php echo $benefits_count; ?>">
												</div>
											</div>
										<?php } ?>
									</div>
								<?php } else if ($benefit['image']) { ?>
									<div class="col-md-12 text-center">
										<img src="<?php echo $benefit['image']; ?>" alt="Img - <?php echo $benefits_count; ?>">
									</div>
								<?php } else if ($benefit['content']) { ?>
									<div class="col-lg-6 offset-md-3">
										<div class="benefit-table">
											<div class="benefit-tableCell page-wysiwig">
												<?php echo $benefit['content']; ?>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
							
						<?php 
							$benefits_count++;
						} ?>
					</div>
					<?php } ?>
					<?php if ($product_parameters) { ?>
						<div id="parameter" class="page-tabNavSection">
							<div class="row">
								<div class="col-12">
									<div class="title-borderOuter">
										<h2 class="title-border">Parametry produktu</h2>
									</div>
								</div>
							</div>
							
							<div class="page-wysiwig <?php if ($product_parametersSticky) { echo 'sticky-table-header'; } ?>">
								<?php echo wpautop($product_parameters); ?>
							</div>
						</div>
					<?php } ?>
					<!-- <div id="application_of_the_image" class="page-tabNavSection">Galeria</div>
					<div id="video" class="page-tabNavSection">Wideo</div> -->
				</div>
			</div>

			<?php if (sensmart_get_option('is_camera')) { ?>
				
				<?php hm_get_template_part('template-form', [
					'message' => get_the_title()
					]
				); ?>

			<?php } ?>

			<?php hm_get_template_part('templates/template-products-similar', [
				'limit' => 4,
				'taxonomy' => $product_type,
				'excludeId' => $postID
			]); ?>
			
		<?php endwhile; ?>
		<?php endif; ?>
	</article>
</main>

<?php get_footer(); ?>