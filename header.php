<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	
	<title>
		<?php wp_title(''); ?>
	</title>
	
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href="//www.google-analytics.com" rel="dns-prefetch">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,500,700&display=swap&subset=latin-ext" rel="stylesheet">

   	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css" media="screen">

	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon.ico" />

	<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/img/facebook-share.jpg" />
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-542S32S');</script>
	<!-- End Google Tag Manager -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-162080947-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-162080947-1');
	</script>

	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '1152302728480475');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=1152302728480475&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

	<?php wp_head(); ?>
</head>

<body <?php body_class('loading '. ($_COOKIE["fontSize"] ? 'fontSize-'.$_COOKIE["fontSize"] : '' ) ); ?>>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-542S32S"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<header class="header">
		<div class="header-switcher">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul>
							<li>
								<a class="d-none d-md-block" href="#" title="Wybrany dział">Wybrany dział:</a>
							</li>
							<li class="<?php if (!sensmart_get_option('is_camera')) { echo 'active'; } ?>">
								<a title="Sklep z termowizją online - guideir.com.pl" href="https://guideir.com.pl">Produkty outdoorowe</a>
							</li>
							<li class="<?php if (sensmart_get_option('is_camera')) { echo 'active'; } ?>"> 
								<a title="Kamery termowizjne do masowego pomiaru temperatury"  href="https://guideir.com.pl/kamery-termowizyjne">Kamery termowizyjne</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="header-logo">
						<a href="<?php echo get_home_url(); ?>" title="Termowizja Sklep Online - Guide Sensmart">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="Logo Guide Sensmart">
						</a>
					</div>
					
					<div class="header-navTrigger">
						<a href="#">
							<span class="line-1"></span>
							<span class="line-2"></span>
							<span class="line-3"></span>
						</a>
					</div>
					
					<div class="header-nav">
						<nav>
							<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
						</nav>
					</div>

					<div class="header-fontSize">
						<h3>Rozmiar tekstu:</h3>
						<ul>
							<li class="<?php if ($_COOKIE["fontSize"] !== "1"  && $_COOKIE["fontSize"] !== "2") { echo 'active'; } ?>">
								<a data-size="0" href="#">A</a>
							</li>
							<li class="<?php if ($_COOKIE["fontSize"] && $_COOKIE["fontSize"] == "1") { echo 'active'; } ?>">
								<a data-size="1" href="#">A+</a>
							</li>
							<li class="<?php if ($_COOKIE["fontSize"] && $_COOKIE["fontSize"] == "2") { echo 'active'; } ?>">
								<a data-size="2" href="#">A++</a>
							</li>
						</ul>
					</div>

					<a href="<?php echo get_home_url(); ?>/?s=" class="header-search">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-search.svg" alt="Search">
					</a>
				</div>
			</div>
		</div>
	</header>

	<!-- <div class="page-selectBranch <?php if ($_COOKIE["website"] || !is_front_page()) { echo 'hidden'; } ?>">
		<div class="page-selectBranch__inner">
			<div class="row">
				<a class="page-selectBranch__logo" href="<?php echo get_home_url(); ?>" title="Termowizja Sklep Online - Guide Sensmart">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="Logo Guide Sensmart">
				</a>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h3>Wybierz dział:</h3>
				</div>
			</div>
			<div class="row">

				<?php if (sensmart_get_option('is_camera')) { ?>
				<div class="col-12 col-sm-6">
					<a class="page-selectBranch__item <?php if (sensmart_get_option('is_camera')) { echo 'active'; } ?>" data-website="kamery" href="https://guideir.com.pl/kamery-termowizyjne">
						<span class="page-selectBranch__item__bg kamery"></span>
						<span class="page-selectBranch__item__text">
							Kamery termowizyjne
						</span>
					</a>
				</div>
				<?php } ?>
				
				<div class="col-12 col-sm-6">
					<a class="page-selectBranch__item <?php if (!sensmart_get_option('is_camera')) { echo 'active'; } ?>" data-website="lowiectwo" href="https://guideir.com.pl">
						<span class="page-selectBranch__item__bg lowiectwo"></span>
						<span class="page-selectBranch__item__text">Produkty outdoorowe</span>
					</a>
				</div>

				<?php if (!sensmart_get_option('is_camera')) { ?>
				
				<div class="col-12 col-sm-6 <?php if (sensmart_get_option('is_camera')) { ?>col-md-pull-6<?php } ?>">
					<a class="page-selectBranch__item <?php if (sensmart_get_option('is_camera')) { echo 'active'; } ?>" data-website="kamery" href="https://guideir.com.pl/kamery-termowizyjne">
						<span class="page-selectBranch__item__bg kamery"></span>
						<span class="page-selectBranch__item__text">
							Kamery termowizyjne
						</span>
					</a>
				</div>
				<?php } ?>
			</div>
		</div>
	</div> -->