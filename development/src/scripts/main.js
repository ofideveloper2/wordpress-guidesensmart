import $ from 'jquery'
import imagesLoaded from 'imagesLoaded'
import { TweenLite } from 'gsap'

import '../../node_modules/gsap/src/uncompressed/plugins/ScrollToPlugin.js'
import '../../node_modules/slick-carousel/slick/slick.min.js'

import WOW from 'wowjs'

new WOW.WOW({
	offset: window.innerHeight * 0.1
}).init();

// var Parsley = require('parsleyjs');
// var Hammer = require('hammerjs');

/*!
 * The Final Countdown for jQuery v2.2.0 (http://hilios.github.io/jQuery.countdown/)
 * Copyright (c) 2016 Edson Hilios
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
!function(a){"use strict";"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){"use strict";function b(a){if(a instanceof Date)return a;if(String(a).match(g))return String(a).match(/^[0-9]*$/)&&(a=Number(a)),String(a).match(/\-/)&&(a=String(a).replace(/\-/g,"/")),new Date(a);throw new Error("Couldn't cast `"+a+"` to a date object.")}function c(a){var b=a.toString().replace(/([.?*+^$[\]\\(){}|-])/g,"\\$1");return new RegExp(b)}function d(a){return function(b){var d=b.match(/%(-|!)?[A-Z]{1}(:[^;]+;)?/gi);if(d)for(var f=0,g=d.length;f<g;++f){var h=d[f].match(/%(-|!)?([a-zA-Z]{1})(:[^;]+;)?/),j=c(h[0]),k=h[1]||"",l=h[3]||"",m=null;h=h[2],i.hasOwnProperty(h)&&(m=i[h],m=Number(a[m])),null!==m&&("!"===k&&(m=e(l,m)),""===k&&m<10&&(m="0"+m.toString()),b=b.replace(j,m.toString()))}return b=b.replace(/%%/,"%")}}function e(a,b){var c="s",d="";return a&&(a=a.replace(/(:|;|\s)/gi,"").split(/\,/),1===a.length?c=a[0]:(d=a[0],c=a[1])),Math.abs(b)>1?c:d}var f=[],g=[],h={precision:100,elapse:!1,defer:!1};g.push(/^[0-9]*$/.source),g.push(/([0-9]{1,2}\/){2}[0-9]{4}( [0-9]{1,2}(:[0-9]{2}){2})?/.source),g.push(/[0-9]{4}([\/\-][0-9]{1,2}){2}( [0-9]{1,2}(:[0-9]{2}){2})?/.source),g=new RegExp(g.join("|"));var i={Y:"years",m:"months",n:"daysToMonth",d:"daysToWeek",w:"weeks",W:"weeksToMonth",H:"hours",M:"minutes",S:"seconds",D:"totalDays",I:"totalHours",N:"totalMinutes",T:"totalSeconds"},j=function(b,c,d){this.el=b,this.$el=a(b),this.interval=null,this.offset={},this.options=a.extend({},h),this.instanceNumber=f.length,f.push(this),this.$el.data("countdown-instance",this.instanceNumber),d&&("function"==typeof d?(this.$el.on("update.countdown",d),this.$el.on("stoped.countdown",d),this.$el.on("finish.countdown",d)):this.options=a.extend({},h,d)),this.setFinalDate(c),this.options.defer===!1&&this.start()};a.extend(j.prototype,{start:function(){null!==this.interval&&clearInterval(this.interval);var a=this;this.update(),this.interval=setInterval(function(){a.update.call(a)},this.options.precision)},stop:function(){clearInterval(this.interval),this.interval=null,this.dispatchEvent("stoped")},toggle:function(){this.interval?this.stop():this.start()},pause:function(){this.stop()},resume:function(){this.start()},remove:function(){this.stop.call(this),f[this.instanceNumber]=null,delete this.$el.data().countdownInstance},setFinalDate:function(a){this.finalDate=b(a)},update:function(){if(0===this.$el.closest("html").length)return void this.remove();var b,c=void 0!==a._data(this.el,"events"),d=new Date;b=this.finalDate.getTime()-d.getTime(),b=Math.ceil(b/1e3),b=!this.options.elapse&&b<0?0:Math.abs(b),this.totalSecsLeft!==b&&c&&(this.totalSecsLeft=b,this.elapsed=d>=this.finalDate,this.offset={seconds:this.totalSecsLeft%60,minutes:Math.floor(this.totalSecsLeft/60)%60,hours:Math.floor(this.totalSecsLeft/60/60)%24,days:Math.floor(this.totalSecsLeft/60/60/24)%7,daysToWeek:Math.floor(this.totalSecsLeft/60/60/24)%7,daysToMonth:Math.floor(this.totalSecsLeft/60/60/24%30.4368),weeks:Math.floor(this.totalSecsLeft/60/60/24/7),weeksToMonth:Math.floor(this.totalSecsLeft/60/60/24/7)%4,months:Math.floor(this.totalSecsLeft/60/60/24/30.4368),years:Math.abs(this.finalDate.getFullYear()-d.getFullYear()),totalDays:Math.floor(this.totalSecsLeft/60/60/24),totalHours:Math.floor(this.totalSecsLeft/60/60),totalMinutes:Math.floor(this.totalSecsLeft/60),totalSeconds:this.totalSecsLeft},this.options.elapse||0!==this.totalSecsLeft?this.dispatchEvent("update"):(this.stop(),this.dispatchEvent("finish")))},dispatchEvent:function(b){var c=a.Event(b+".countdown");c.finalDate=this.finalDate,c.elapsed=this.elapsed,c.offset=a.extend({},this.offset),c.strftime=d(this.offset),this.$el.trigger(c)}}),a.fn.countdown=function(){var b=Array.prototype.slice.call(arguments,0);return this.each(function(){var c=a(this).data("countdown-instance");if(void 0!==c){var d=f[c],e=b[0];j.prototype.hasOwnProperty(e)?d[e].apply(d,b.slice(1)):null===String(e).match(/^[$A-Z_][0-9A-Z_$]*$/i)?(d.setFinalDate.call(d,e),d.start()):a.error("Method %s does not exist on jQuery.countdown".replace(/\%s/gi,e))}else new j(this,b[0],b[1])})}});

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

$.fn.removeClassStartingWith = function (filter) {
    $(this).removeClass(function (index, className) {
        return (className.match(new RegExp("\\S*" + filter + "\\S*", 'g')) || []).join(' ')
    });
    return this;
};

var App = function() {

	var W = $(window).width(),
		H = $(window).height(),
		scrollTop = $(window).scrollTop(),
		isMobileNavOpen = false,
		isHome = $('body.home').length > 0,
		sliderInstance = {},
		$window = $(window),
		$body = $('body')

	let setCookie = function(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+ d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}

	let getCookie = function(cname) {
		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}

			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}
	
	let setupEvents = function() {
		var _this = this;

		$('form').on('click', '.button', function(event) {
			if (!$(this).hasClass('search')) {
				event.preventDefault();
				$(this).submit();
			}
		});

		$('form').on('submit', function(event) {

			if (!$(this).hasClass('search')) {
			
				event.preventDefault();

				var $this = $(this),
					action = $(this).attr('action'),
					data = $(this).serialize(),
					formId = $this.attr('id'),
					$rowMessage = $('.row-message');

				action = action.replace('_submit', '');
					
				if (!isCaptcha()) {
					return false
				}

				$.ajax({
					url: action,
					type: 'POST',
					cache: false,
					data: data,

					beforeSend: function() {
						$rowMessage.addClass('hidden')
						$('.row-submit').addClass('hidden')
						$('.row-preloader').removeClass('hidden')
					},

					success : function(res) {
						res = JSON.parse(res)
						$('.row-preloader').addClass('hidden')

						if (formId == 'request' && window.gtag) {
							var url = undefined
					
							var callback = function () {
								if (typeof(url) != 'undefined') {
									window.location = url;
								}
							};

							window.gtag('event', 'Formularz - zapytanie o produkt', {
								event_category: 'Formularz - zapytanie o produkt',
								event_label: 'Formularz - zapytanie o produkt'
							})
						} else {
							window.gtag('event', 'Formularz - kontakt', {
							event_category: 'Formularz - kontakt',
							event_label: 'Formularz - kontakt'
							})
						}

						if (res.status == true) {
							var successHTML = $rowMessage.find('.success').html()

							$rowMessage.removeClass('hidden')
							$rowMessage.find('.error').addClass('hidden')

							if ($('#name').get(0).value == '') {
								$rowMessage.find('.success').html( successHTML.replace('{name}', '') )
							} else {
								$rowMessage.find('.success').html( successHTML.replace('{name}', $('#name').get(0).value) )
							}

							$('.form-body').remove()
							
							$rowMessage.find('.success').removeClass('hidden')

							scrollTo({
								pos: $rowMessage.parent().parent().offset().top - 60 - $('.header').height()
							})
						}

						if (res.status == false) {
							$rowMessage.removeClass('hidden')
							$rowMessage.find('.error').removeClass('hidden')
							$rowMessage.find('.success').addClass('hidden')
							$('.row-submit').removeClass('hidden')

							setTimeout(function() {
								$('.row-preloader').addClass('hidden')
								$rowMessage.addClass('hidden')
							}, 3000)
						}
					},
					error: function() {
						$rowMessage.removeClass('hidden')
						$rowMessage.find('.error').removeClass('hidden')
						$rowMessage.find('.success').addClass('hidden')
						$('.row-submit').removeClass('hidden')
						$('.row-preloader').addClass('hidden')

						setTimeout(function() {
							$('.row-preloader').addClass('hidden')
							$rowMessage.addClass('hidden')
						}, 3000)
					}
				})
			}
		});

		$('.header-navTrigger a').click(function(event) {
			event.preventDefault()
			toggleMobileNav()
		});

		$('.button-back').on('click', function(event) {
			event.preventDefault()
			window.history.back()
		})

		$('.page-tabNav li a').on('click', function(event) {
			event.preventDefault();
			var show_id = $(this).data('show');

			$('.page-tabNav li').removeClass('active');
			$(this).parent().addClass('active');

			$('.page-tabNavSection').removeClass('active');
			$('.page-tabNavSection#' + show_id).addClass('active');
		})

		$('.page-tabNav li').first().find('a').trigger('click');

		$('a[href*="#"]')
		// Remove links that don't actually link to anything
		.not('[href="#"]')
		.not('[href="#0"]').on('click', function(event) {
			event.preventDefault();
			var target = $(this.hash);
			event.preventDefault();

			scrollTo({
				pos: target.offset().top
			});
		});

		$window.on('resize', function(event) {
			onResize()
		});

		$window.on('scroll', function(event) {
			onScroll();
		});

		$('.input-sku').on('keyup', function(event) {
			let $skusMessages = $('.skus-messages')

			$skusMessages.addClass('hidden')
			$skusMessages.find('p').fadeOut()

			if (event.keyCode == 13) {
				$('.btn-skuSearch').trigger('click')
			}
		})

		$('.btn-skuSearch').on('click', function(event) {
			event.preventDefault()
			
			let sku = $('.input-sku').val(),
				data = {
					request_sku: sku.toLowerCase()
				},
				action = $('.input-sku').data('action'),
				$skusMessages = $('.skus-messages')

			action = action.replace('check_', '')

			if (sku.length > 6) {

				$.ajax({
					url: action,
					type: 'POST',
					cache: false,
					data: data,

					success: function(_res) {
						let res = JSON.parse(_res)

						if (res.status == true) {
							$skusMessages.removeClass('hidden')
							$skusMessages.find('.skus-messages__success').fadeIn('hidden')
						}

						if (res.status == false) {
							$skusMessages.removeClass('hidden')
							$skusMessages.find('.skus-messages__error').fadeIn('hidden')
						}
					},
					error: function() {
						$skusMessages.removeClass('hidden')
						$skusMessages.find('.skus-messages__api_error').fadeIn('hidden')
					},
					complete: function() {
						scrollTo({
							pos: $skusMessages.offset().top - 100
						})
					}
				})
				
			} else {
				$skusMessages.removeClass('hidden')
				$skusMessages.find('.skus-messages__tooShort').fadeIn('hidden')

				scrollTo({
					pos: $skusMessages.offset().top - 100
				})
			}
		})

		$('.header-fontSize').on('click', 'a', function(event) {
			event.preventDefault()

			let $this = $(this),
				dataSize = $this.data('size')

			$('.header-fontSize li').removeClass('active')
			$this.parent().addClass('active')

			$('body').removeClassStartingWith('fontSize-')

			$('body').addClass('fontSize-'+dataSize)

			setCookie('fontSize', dataSize, 10)			
		})

		$('.page-selectBranch__item').on('click',function(event) {
			event.preventDefault()
		  
			let $this = $(this),
				dataWebsite = $this.data('website')
		  
			setCookie('website', dataWebsite, 1)
			
			if ($this.hasClass('active')) {
				$('.page-selectBranch').fadeOut();
			} else {
				window.location = $this.attr('href');
			}
		})

		$('body').on('click', '.page-shops__mapMoreClose', function(event) {
			event.preventDefault();
			$(this).parent().removeClass('visible');

			if (window.activeMarker && window.activeMarker.getAnimation() !== null) {
				window.activeMarker.setAnimation(null);
			}

			window.activeMarker = undefined;
		});
	}

	let highlightMenu = function() {
		$('.single-product .menu-item-productsNav').addClass('current-menu-item');
	}

	let onResize = function() {
		W = window.innerWidth
		H = window.innerHeight
		adjustHeight()
		stickyGallery()
	}

	let adjustHeight = function() {
		adjustItemHeight( $('.product-preview-image') )
		adjustItemHeight( $('.product-gallery .benefit-table') )
	}

	let onScroll = function() {
		scrollTop = $window.scrollTop()

		stickyTableHeader()
		stickyProductPopup()
		stickyGallery()
		stickyMap()
	}

	let scrollToTop = function() {
		if ($window.scrollTop() > 0) {
			scrollTo({
				pos: 0, 
				speed: 0.4
			});
		}
	}

	let adjustItemHeight = function($selector) {
		let $item = $selector,
			_max = 100

		$item.css('min-height', _max)
		
		$item.each(function(index, $item) {
			let _height = $(this).outerHeight()

			if (_height > _max) {
				_max = _height
			}
		})

		$item.each(function(index) {
			$(this).css('min-height', _max)
		})
	}

	var toggleMobileNav = function() {
		var $header = $('header'),
			$nav = $('.header-navTrigger'),
			$pageContent = $('.page-content');

		if ($header.hasClass('active')) {
			$header.removeClass('active');
			$header.removeClass('active');
			$nav.removeClass('active');
		} else {
			$header.addClass('active');
			$nav.addClass('active');
		}
	}

	var scrollTo = function(params) {
		if (params.pos == undefined) {params.pos = 0};
		if (params.speed == undefined) {params.speed = 0.8};

		TweenLite.to( window, params.speed, {
			scrollTo: {
				y: params.pos - $('.header').height() - 20
			},
			ease: Cubic.EaseIn
		});
	}

	let preload = function(images) {
		if (images) {
			$.each(images, function(index, val) {
				$body.find('.footer').append('<img class="preloading" src=" ' + val +' ">');
			});
		}

		imagesLoaded( 'body', { background: true }, function() {
			$body.find('.preloading').each(function(index, el) {
				$(this).remove()
			});

			$body.removeClass('loading')

			adjustHeight();
		});
	}

	let isCaptcha = function() {
		let captcha_code = window.grecaptcha.getResponse();
		let is_valid = captcha_code !== '' ? true : false;

		if (!is_valid) {
			$('.captchaHolder-error').removeClass('hidden');
		} else {
			$('.captchaHolder-error').addClass('hidden');
		}
		
		return is_valid;
	}

	let stickyTableHeader = function() {
		if ($('.stickyHeader').length > 0) {
			let offsetTop = $('.stickyHeader').offset().top,
				$firstRow = $('.stickyHeader tr').first(),
				_height = $('.stickyHeader').height();

			var move = (offsetTop - scrollTop - $('.header').height() - 40).toFixed(0)
			var stopStatement = scrollTop < offsetTop + _height - 300
	
			if (move < 0 && stopStatement) {
				TweenLite.to($firstRow, 0.01, {
					y: Math.abs(move)
				})
			} else if (move > 0) {
				TweenLite.to($firstRow, 0.01, {
					y: 0
				})
			}
		}
	}

	let stickyGallery = function() {
		let $gallery = $('.product-gallery-outer'),
			$productInfo = $('.product-info')

			if (W >= 768 && $gallery.length > 0 && $productInfo.length > 0) {
				let offsetTop = $productInfo.offset().top,
					_height = $productInfo.height()
	
				var move = (offsetTop - scrollTop - $('.header').height() - 80)
				var stopStatement = scrollTop < offsetTop + _height - $('.header').height() - 80 - $gallery.height()
		
				if (move < 0 && stopStatement) {
					TweenLite.to($gallery, 0, {
						y: Math.abs(move)
					})
				} else if (move > 0) {
					TweenLite.to($gallery, 0, {
						y: 0
					})
				}
			}

			if (W < 768) {
				TweenLite.to($gallery, 0, {
					y: 0
				})
			}
	}

	let stickyMap = function() {
		
		if ($('.page-shops__map').length > 0) {
			let offsetTop = $('.page-shops__map').offset().top,
				$map = $('.page-shops__map'),
				_height = $('.page-shops__mapOuter').height();

			var move = (offsetTop - scrollTop - $('.header').height() - 40).toFixed(0)
			var stopStatement = scrollTop < offsetTop + _height - 300
	
			if (move < 0 && stopStatement) {
				$map.addClass('pinned')
				
				// TweenLite.to($map, 0.01, {
				// 	y: Math.abs(move)
				// })
			} else if (move > 0) {
				$map.removeClass('pinned')

				// TweenLite.to($map, 0.01, {
				// 	y: 0
				// })
			}
		}
	}

	let stickyProductPopup = function() {
		let $parameters = $('#parameter')

		if ($('.product-popup').length > 0 && $parameters.length > 0) {
			let paramsOffset = $parameters.offset().top,
				start = scrollTop > paramsOffset,
				stop = scrollTop < paramsOffset + $parameters.height() - 200

			if (start && stop) {
				$('.product-popup').addClass('active')	
			} else {
				$('.product-popup').removeClass('active')	
			}
		}
	}

	let wrapTables = function() {
		$('table').each(function() {
			let $this = $(this)

			$this.wrap('<div class="table-wrapper"></div>')
		})
	}

	let showShop = function(id) {

		$('.page-shops__mapMore').addClass('visible');
		var _content = $('.page-shops__preview[data-shopid="'+ id +'"]').parent().html()
		$('.page-shops__mapMoreContent').html(_content);
	}
	
	let init = function() {
		
		setupEvents();
		preload([]);
		
		highlightMenu();

		wrapTables();

		$('.single-dystrybutorzy .menu-item-dystrybutorzy').addClass('current-menu-item')
		
		$('.sticky-table-header table').addClass('stickyHeader')

		$('.slider').slick({
			autoplay: true,
			speed: 600,
			autoplaySpeed: 5500,
			fade: false,
			dots: true,
			accessibility: false,
			pauseOnHover: false,
			pauseOnDotsHover: true,
			arrows : false
		});

		$('.product-gallery').slick({
			autoplay: true,
			speed: 600,
			autoplaySpeed: 3500,
			fade: false,
			dots: true,
			accessibility: false,
			pauseOnHover: false,
			pauseOnDotsHover: true,
			arrows: true
		});
	}

	init()

	return {
		isCaptcha: isCaptcha,
		showShop: showShop
	}
}

$(document).ready(function() {
	let app = new App()
	window.captchaLoadCallback = app.isCaptcha;
	window.showShop = app.showShop;
});
