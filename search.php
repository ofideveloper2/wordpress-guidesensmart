<?php get_header(); ?>

<?php get_template_part('template-breadcrumbs'); ?>

<main role="main" class="page-content page-form">
	<section class="container">
		<div class="row">
			<div class="col-12">
				<div class="title-borderOuter">
					<h2 class="title-border mt-0">Szukaj</h2>
				</div>
				
				<?php get_template_part('searchform'); ?>
			</div>
		</div>

		<?php if ( $_GET['s'] && !empty($_GET['s']) ) { ?>
		
			<div class="row">
				<div class="col-12">
					<div class="title-borderOuter">
						<h3 class="title-border">
							<?php echo sprintf( __( '%s wyników wyszukiwania dla "', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?>"
						</h3>
					</div>
				</div>
			</div>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>
		<?php } ?>
	</section>
</main>

<?php get_footer(); ?>

