<?php 

require_once(dirname(__FILE__) . '/wp-config.php');
$wp->init();
$wp->parse_request();
$wp->query_posts();
$wp->register_globals();
$wp->send_headers();

$skus = sensmart_get_option('skus');
$isMatch = false;

if ($skus) {
    $skus = preg_replace('#\s+#',',',trim($skus));
    $skus_array = explode(",", $skus);
    
    if ($_POST['request_sku'] && $_POST['request_sku'] !== '') {
        $request_sku = $_POST['request_sku'];

        foreach ($skus_array as $sku) {
            if ($request_sku == strtolower($sku)) {
                $isMatch = true;
            }
        }
    }
}

if ($isMatch) {
	print_r('{"status":true}');
} else {
	print_r('{"status":false}');
}


?>
