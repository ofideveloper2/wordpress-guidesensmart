<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php if ( has_post_thumbnail()) : ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_post_thumbnail(array(120,120)); ?>
			</a>
		<?php endif; ?>

		<p class="mb-0">
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_title(); ?> &rarr;
			</a>
		</p>
		<!-- <span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span> -->
		<?php edit_post_link('Edytuj'); ?>
	</article>
<?php endwhile; ?>
<?php else: ?>
	<article>
		<p><?php _e( 'Przepraszamy, brak wyników.', 'html5blank' ); ?></p>
		<a href="<?php echo get_home_url(); ?>">Wróć do strony głównej &rarr;</a>
	</article>
<?php endif; ?>
