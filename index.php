<?php get_header(); ?>
<main role="main" class="page-content">
	<div class="page-content-mask"></div>
	
	<?php get_template_part('template-slider'); ?>

	<div class="wrapper">
		<section>
			<?php get_template_part('loop'); ?>
			<?php get_template_part('pagination'); ?>
		</section>
	</div>
</main>
<?php get_footer(); ?>
